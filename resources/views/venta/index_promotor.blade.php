@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script>
    $(function(){
        $('table').DataTable({
            'language' : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });

        $('.borrarVenta').click(function (element) {
            element.preventDefault();
            if(confirm("¿Eliminar venta " + $(this).attr("venta_id") + "?")){
                $('#venta' + $(this).attr("venta_id")).submit();
            }
        });
    });
</script>
@endsection

@section('content')
<div class="row  justify-content-center">
    <div class="col-md-7 mb-3">
        <div class="card">
            <div class="card-header">
                Dar de alta vendedor
            </div>
            <div class="card-body">
                <form action="/usuario" method="post">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label>Nombre</label>
                            <input name="nombre" type="text" class="form-control form-control-sm" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Apellido paterno (opcional)</label>
                            <input name="apellidoPaterno" type="text" class="form-control form-control-sm">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Apellido materno (opcional)</label>
                            <input name="apellidoMaterno" type="text" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Correo electrónico</label>
                        <input name="email" type="email" class="form-control form-control-sm" required>
                    </div>
                    <p>Se enviará una contraseña segura al correo del vendedor.</p>
                    <button class="btn btn-sm btn-success">Inscribir</button>
                </form>
            </div>
        </div>
    </div>

    @if (App\User::where('promotor_id', Auth::id())->count() > 0)
    <div class="col-md-7 mb-3">
        <div class="card">
            <div class="card-header">
                Vendedores
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>Vendedor</th>
                                <th class="text-center">Ventas</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (App\User::where('promotor_id', Auth::id())->get() as $u)
                            <tr>
                                <td>{{ "$u->nombre $u->apellidoPaterno $u->apellidoMaterno" }}</td>
                                <td class="text-center">{{ $u->ventas->where('cantidad', '!=', -1)->count() }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                <i class="fas fa-file-invoice"></i> Ventas
            </div>
            <div class="card-body">
                <p>Comparte este enlace para que tus clientes puedan registrarse a tu nombre:</p>
                <p class="text-center"><a href="http://mexicanoseguro.com/registro/{{Auth::id()}}">http://mexicanoseguro.com/registro/{{Auth::id()}}</a></p>

                <div class="table-responsive">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Vendedor</th>
                                <th>Cliente</th>
                                <th>Fecha de venta</th>
                                <th>Periodo</th>
                                @if(Auth::user()->role_id != 3)
                                <th>Acciones</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                        @if(isset($ventas))
                        @foreach($ventas as $v)
                        @if (App\User::withTrashed()->find($v->cliente_id) == null)
                            @continue
                        @endif
                        @if($v->cantidad == -1) {{-- Evitamos mostrar ventas sólo asociadas a vendedor --}}
                            @continue
                        @endif
                        <tr>
                            <td>{{$v->id}}</td>
                            <td>{{$v->vendedor ? $v->vendedor->nombre : '---'}}</td>
                            <td>{{App\User::withTrashed()->find($v->cliente_id)->nombre}}</td>
                            <td>{{$v->created_at->format('d/m/y')}}</td>
                            <td>{{Carbon\Carbon::parse($v->desde)->isoFormat('DD MMM')}} - {{Carbon\Carbon::parse($v->hasta)->isoFormat('DD MMM')}}</td>
                            {{--<td>{{\Carbon\Carbon::create($v->desde)->format('d/m/y')}} - {{\Carbon\Carbon::create($v->hasta)->format('d/m/y')}}</td>--}}
                            @if(Auth::user()->role_id != 3)
                            <td class="text-right">
                                <a href="#" class="btn btn-danger btn-sm borrarVenta" venta_id="{{$v->id}}">Borrar</a>
                                <form id="venta{{$v->id}}" action="/venta/{{$v->id}}" method="POST" style="display: none;">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection