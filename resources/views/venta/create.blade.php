@extends('layouts.app')

@section('styles')
    <style>
        /**
        * Shows how you can use CSS to style your Element's container.
        */
        .MyCardElement {
        height: 40px;
        padding: 10px 12px;
        width: 100%;
        color: #32325d;
        background-color: white;
        border: 1px solid transparent;
        border-radius: 4px;

        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
        }

        .MyCardElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .MyCardElement--invalid {
        border-color: #fa755a;
        }

        .MyCardElement--webkit-autofill {
        background-color: #fefde5 !important;
        }
    </style>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script src="https://www.paypal.com/sdk/js?client-id={{ env('PAYPAL_ID') }}&currency={{$currency}}&disable-funding=card">
</script>
<script src="https://js.stripe.com/v3/"></script>
<script>
    $(function() {
        clientSecret = '{{$client_secret}}';


        stripe = Stripe('{{ env('STRIPE_PUPLIC') }}');
        var elements = stripe.elements();

        var style = {
            base: {
                color: "#32325d",
            }
        };

        var card = elements.create("card", { style: style });
        card.mount("#card-element");

        card.on('change', ({error}) => {
            const displayError = document.getElementById('card-errors');
            if (error) {
                displayError.textContent = error.message;
            } else {
                displayError.textContent = '';
            }
        });

        var form = document.getElementById('payment-form');

        form.addEventListener('submit', function(ev) {
            ev.preventDefault();
            stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                    card: card,
                    billing_details: {
                        name: '{{Auth::user()->nombre}}'
                    }
                }
            }).then(function(result) {
                if (result.error) {
                // Show error to your customer (e.g., insufficient funds)
                console.log(result.error.message);
                } else {
                // The payment has been processed!
                if (result.paymentIntent.status === 'succeeded') {
                    // Show a success message to your customer
                    // There's a risk of the customer closing the window before callback
                    // execution. Set up a webhook or plugin to listen for the
                    // payment_intent.succeeded event that handles any business critical
                    // post-payment actions.
                    $('#firma').val(signaturePad.toDataURL());
                    $('#registro').submit();
                }
                }
            });
        });

        var canvas = document.querySelector("canvas");
        signaturePad = new SignaturePad(canvas);

        $('#borrarFirma').click(function(e){
            e.preventDefault();
            signaturePad.clear();
        });
        
        $('.requeridos').change(function(){
            var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
            if ($('#nombre').val() != '' && pattern.test($('#email').val())){
                $('#paypal-buttons').show();
            }
        });

        paypal.Buttons({
            createOrder: function(data, actions) {
                // This function sets up the details of the transaction, including the amount and line item details.
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: '{{$plan->mensualidad + $plan->sdi}}'
                        }
                    }]
                });
            },
            onApprove: function(data, actions) {
                // This function captures the funds from the transaction.
                return actions.order.capture().then(function(details) {
                    // This function shows a transaction success message to your buyer.
                    $('#firma').val(signaturePad.toDataURL());
                    $('#registro').submit();
                });
            }
        }).render('#paypal-button-container');
    });
</script>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h3>Nueva venta</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 text-center my-5">
                    <h1 class="text-center">Por solo</h1>
                    <h1 class="font-weight-bold display-3">
                        @if (strpos ($plan->mensualidad, '.') !== false)
                        ${{substr((string)$plan->mensualidad, 0, strpos ((string)$plan->mensualidad, '.') + 1)}}<sup><small style="font-size:2rem">{{strpos ((string)$plan->mensualidad, '.') !== false ? substr($plan->mensualidad, strpos ($plan->mensualidad, '.') + 1) : ''}}</small></sup> <small>{{ $currency }}</small>
                        @else
                        ${{$plan->mensualidad}} <small>{{ $currency }}</small>
                        @endif
                        </h1>
                    <small class="lead m-0">al mes</small>
                </div>
                <div class="col-md-6 my-5 container d-flex align-items-center">
                    <div>
                        <h4><i class="fas fa-check-square"></i> <b>Cuota única</b> de inscripción
                        @if (strpos ($plan->sdi, '.') !== false)
                        ${{substr($plan->sdi, 0, strpos ($plan->sdi, '.') + 1)}}<sup><small style="font-size:0.6rem">{{strpos ($plan->sdi, '.') !== false ? substr($plan->sdi, strpos ($plan->sdi, '.') + 1) : ''}}</small></sup> <small>{{ $currency }}</small>
                        @else
                        ${{$plan->sdi}} <small>{{ $currency }}</small>
                        @endif
                        </h4>
                        <h4><i class="fas fa-check-square"></i> <b>Protege</b> a tu familia</h4>
                        <h4><i class="fas fa-check-square"></i> <b>Sin trámites</b> largos ni plazos forzosos</h4>
                        <h4><i class="fas fa-check-square"></i> <b>Rápida y eficiente</b> atención a clientes</h4>
                    </div>
                </div>
            </div>
            <form method="POST" action="/venta" id="registro" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="firma" id="firma">
                <input type="hidden" name="stripe" id="stripe" value="0">
                <input type="hidden" name="payment_method" id="payment_method">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <small for="nombre" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Nombre(s)</small>
                        <input type="text" class="form-control requeridos form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre') }}" required autofocus>
                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <small for="apellidoPaterno" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Apellido Paterno (opcional)</small>
                        <input type="text" class="form-control form-control{{ $errors->has('apellidoPaterno') ? ' is-invalid' : '' }}" id="apellidoPaterno" name="apellidoPaterno" value="{{ old('apellidoPaterno') }}">
                        @if ($errors->has('apellidoPaterno'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellidoPaterno') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <small for="apellidoMaterno" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Apellido Materno (opcional)</small>
                        <input type="text" class="form-control form-control{{ $errors->has('apellidoMaterno') ? ' is-invalid' : '' }}" id="apellidoMaterno" name="apellidoMaterno" value="{{ old('apellidoMaterno') }}">
                        @if ($errors->has('apellidoMaterno'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellidoMaterno') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <small for="email">Correo electrónico</small>
                        <input type="email" class="form-control requeridos form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <small for="telefono">Teléfono (opcional)</small>
                        <input type="text" class="form-control form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" id="telefono" name="telefono" value="{{ old('telefono') }}" required>
                        @if ($errors->has('telefono'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <small for="curp">CURP (opcional)</small>
                        <input type="text" class="form-control form-control{{ $errors->has('curp') ? ' is-invalid' : '' }}" id="curp" name="curp" value="{{ old('curp') }}" required>
                        @if ($errors->has('curp'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('curp') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <small for="imss">Número de IMSS (opcional)</small>
                        <input type="text" class="form-control form-control{{ $errors->has('imss') ? ' is-invalid' : '' }}" id="imss" name="imss" value="{{ old('imss') }}" required>
                        @if ($errors->has('imss'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('imss') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <small for="nacimiento">Fecha de nacimiento (opcional)</small>
                        <input type="date" timezone="America/Mexico_City" class="form-control form-control{{ $errors->has('nacimiento') ? ' is-invalid' : '' }}" id="nacimiento" name="nacimiento" value="{{ old('nacimiento') }}" required>
                        @if ($errors->has('nacimiento'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nacimiento') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4 mt-4">
                        <input type="file" class="custom-file-input" id="identificacion" name="identificacion" lang="es">
                        <label class="custom-file-label" for="identificacion">Identificación</label>
                    </div>
                    <div class="form-group col-md-4 mt-4">
                        <input type="file" class="custom-file-input" id="identificacion2" name="identificacion2" lang="es">
                        <label class="custom-file-label" for="identificacion2">Identificación 2</label>
                    </div>
                    <div class="form-group col-md-4 mt-4">
                        <input type="file" class="custom-file-input" id="domicilio" name="domicilio" lang="es">
                        <label class="custom-file-label" for="domicilio">Domicilio</label>
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <canvas width="400" height="100" class="text-center border mr-3"></canvas>
                </div>
                <div class="d-flex justify-content-center">
                    <h4 class="text-center">Firma
                        <button id="borrarFirma" class="btn btn-secondary d-flex align-self-center btn-sm">Limpiar</button>
                    </h4>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <div id="paypal-buttons" class="mt-3 mx-auto" style="display: none">
                <form id="payment-form">
                    <div id="card-element">
                        <!-- Elements will create input elements here -->
                    </div>

                    <!-- We'll put the error messages in this element -->
                    <div id="card-errors" role="alert"></div>

                    <button id="submit" class="btn btn-primary my-2 btn-block">Pagar</button>
                </form>
                <div id="paypal-button-container"></div>
            </div>
        </div>
    </div>
</div>
@endsection
