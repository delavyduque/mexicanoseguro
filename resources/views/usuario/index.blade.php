@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script>
    $(function(){
        $('table').DataTable({
            'language' : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });

        $('.borrarUsuario').submit(function (e) {
            e.preventDefault();
            if(confirm("¿Eliminar usuario " + $(this).attr("usuario_id") + "?")){
                $(this).unbind('submit');
                $(this).submit();
            }
        });
    });
</script>
@endsection

@section('content')
<div class="d-flex justify-content-center">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                @if (\Request::is('cliente*'))
                <i class="fas fa-users"></i> Clientes
                @else
                <i class="fas fa-users"></i> Usuarios <a href="/usuario/crear" class="btn btn-primary btn-sm">Nuevo</a>
                @endif
            </div>
            <div class="card-body">
                <div class="table-responsive mb-5">
                    <table class="table table-sm table-striped text-nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                @if (\Request::is('usuario*'))
                                <th>Rol</th>
                                @endif
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Teléfono</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($usuarios as $u)
                        <tr>
                            <td>{{$u->id}}</td>
                            @if (\Request::is('usuario*'))
                            <td>{{$u->role()->first()->name}}</td>
                            @endif
                            <td>{{$u->nombre}}</td>
                            <td>{{$u->email}}</td>
                            <td>{{$u->telefono}}</td>
                            <td class="text-right">
                                <a href="/usuario/{{$u->id}}/editar" class="btn btn-primary btn-sm">Editar</a>
                                <button class="btn btn-danger btn-sm" form="usuario{{$u->id}}">Borrar</button>
                                <form id="usuario{{$u->id}}" class="borrarUsuario" action="/usuario/{{$u->id}}" method="POST">
                                    {{method_field('DELETE')}}
                                    @csrf
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="d-flex justify-content-center">
    @if (\Request::is('cliente*'))
    <div class="col-md-7 mt-3">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-envelope"></i> Correos
            </div>
            <div class="card-body">
                <div class="list-group">
                    <a href="/mail/1" class="list-group-item list-group-item-action">Bienvenida</a>
                    <a href="/mail/2" class="list-group-item list-group-item-action">Pago nuevo</a>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection