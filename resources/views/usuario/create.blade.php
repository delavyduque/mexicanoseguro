@extends('layouts.app')

@section('scripts')
<script>
    $(function(){
        
    });
</script>
@endsection

@section('content')
<div class="container">
    <h3 class="mt-5">Registrar usuario</h3>
    <form method="POST" action="/usuario">
        @csrf
        <div class="form-group">
            <select class="custom-select{{ $errors->has('rol') ? ' is-invalid' : '' }}" id="rol" name="rol">
                <option selected disabled>Elige un rol</option>
                <option value="1">Admin</option>
                <option value="5">Promotor</option>
                <option value="3">Vendedor</option>
                <option value="4">Cliente</option>
            </select>
            @if ($errors->has('rol'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('rol') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <small for="nombre" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Nombre(s)</small>
                <input type="text" class="form-control form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre') }}" autofocus>
                @if ($errors->has('nombre'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('nombre') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-4">
                <small for="apellidoPaterno" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Apellido Paterno</small>
                <input type="text" class="form-control form-control{{ $errors->has('apellidoPaterno') ? ' is-invalid' : '' }}" id="apellidoPaterno" name="apellidoPaterno" value="{{ old('apellidoPaterno') }}">
                @if ($errors->has('apellidoPaterno'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('apellidoPaterno') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-4">
                <small for="apellidoMaterno" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Apellido Materno</small>
                <input type="text" class="form-control form-control{{ $errors->has('apellidoMaterno') ? ' is-invalid' : '' }}" id="apellidoMaterno" name="apellidoMaterno" value="{{ old('apellidoMaterno') }}">
                @if ($errors->has('apellidoMaterno'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('apellidoMaterno') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <small for="email">Correo electrónico</small>
                <input type="email" class="form-control form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}">
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-6">
                <small for="telefono">Teléfono</small>
                <input type="text" class="form-control form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" id="telefono" name="telefono" value="{{ old('telefono') }}">
                @if ($errors->has('telefono'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('telefono') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <p>Se enviará una clave de acceso generada al azar al correo proporcionado.</p>
        <button class="btn btn-primary" type="submit">Guardar</button>
        <a href="#" onclick="window.history.back()" class="btn btn-secondary">Regresar</a>
    </form>
</div>
@endsection