@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script>
    $(function(){
        $('#ventas').DataTable({
            'language' : {
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ ventas",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "No hay ventas registradas",
                "sInfo":           "Mostrando ventas de la _START_ a la _END_ de un total de _TOTAL_ ventas",
                "sInfoEmpty":      "Mostrando ventas de la 0 a la 0 de un total de 0 ventas",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ ventas)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    });
</script>
@endsection

@section('content')
<div class="container">
    <div class="card mt-3">
        <h5 class="card-header">{{$usuario->nombre}} {{$usuario->apellidoPaterno}} {{$usuario->apellidoMaterno}}</h5>
        <div class="card-body">
            <h3>Información personal</h3>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <th scope="row">Correo-e</th>
                            <td>{{$usuario->email}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Teléfono</th>
                            <td>{{$usuario->telefono}}</td>
                        </tr>
                        <tr>
                            <th scope="row">CURP</th>
                            <td>{{$usuario->curp}}</td>
                        </tr>
                        <tr>
                            <th scope="row">IMSS</th>
                            <td>{{$usuario->imss}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Identificación 1</th>
                            <td><a href="{{Storage::url($usuario->identificacion)}}">Descargar</a></td>
                        </tr>
                        <tr>
                            <th scope="row">Identificación 2</th>
                            <td><a href="{{Storage::url($usuario->identificacion2)}}">Descargar</a></td>
                        </tr>
                        <tr>
                            <th scope="row">Comprobante <br> domicilio</th>
                            <td><a href="{{Storage::url($usuario->domicilio)}}">Descargar</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <a href="/usuario/{{$usuario->id}}/editar" class="btn btn-primary mr-3">Editar</a>
        <a href="#" onclick="window.history.back()" class="btn btn-secondary">Regresar</a>
        @if($usuario->role_id > 2)
        <h3 class="mt-3">Ventas</h3>
        <div class="table-responsive">
            <table class="table table-sm table-striped" id="ventas">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Vendedor</th>
                        <th>Cliente</th>
                        @if(Auth::user()->role_id < 3)
                        <th>Costo</th>
                        @endif
                        <th>Fecha de venta</th>
                        <th>Periodo</th>
                    </tr>
                </thead>
                <tbody>
                @php
                $t = $usuario->role_id == 3 ? 'vendedor_id' : 'cliente_id';
                @endphp
                @foreach(App\Venta::where($t, $usuario->id)->get() as $v)
                <tr>
                    <td>{{$v->id}}</td>
                    <td>{{$v->vendedor ? $v->vendedor->nombre : '---'}}</td>
                    <td>{{App\User->withTrashed()->find($v->cliente_id)->nombre}}</td>
                    @if(Auth::user()->role_id < 3)
                    <td>{{$v->cantidad}}</td>
                    @endif
                    <td>{{$v->created_at->format('d/m/y')}}</td>
                    <td>{{\Carbon\Carbon::create($v->desde)->format('d/m/y')}} - {{\Carbon\Carbon::create($v->hasta)->format('d/m/y')}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        @endif
        </div>
    </div>
</div>
@endsection