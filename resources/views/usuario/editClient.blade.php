@extends('layouts.app')

@section('scripts')
<script>
    $(function(){
        $('#rol').val('{{$usuario->role_id}}');
        $('#nombre').val('{{$usuario->nombre}}');
        $('#apellidoPaterno').val('{{$usuario->apellidoPaterno}}');
        $('#apellidoMaterno').val('{{$usuario->apellidoMaterno}}');
        $('#email').val('{{$usuario->email}}');
        $('#telefono').val('{{$usuario->telefono}}');
        $('#curp').val('{{$usuario->curp}}');
        $('#imss').val('{{$usuario->imss}}');
        $('#nacimiento').val('{{$usuario->nacimiento}}');
        $('#sdi').val('{{$usuario->sdi}}');
        $('#mensualidad').val('{{$usuario->mensualidad}}');
    });
</script>
@endsection

@section('content')
<div class="row d-flex justify-content-center">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                Editar usuario
                <a class="btn btn-primary btn-sm" href="/contrato/{{$usuario->id}}/false">Ver Contrato</a>
            </div>
            <div class="card-body">
                <form method="POST" action="/usuario/{{$usuario->id}}" enctype="multipart/form-data">
                    {{method_field('PUT')}}
                    @csrf
                    <div class="form-group">
                        <select class="custom-select{{ $errors->has('rol') ? ' is-invalid' : '' }}" id="rol" name="rol">
                            <option selected disabled>Elige un rol</option>
                            <option value="2">Admin</option>
                            <option value="3">Vendedor</option>
                            <option value="4">Cliente</option>
                        </select>
                        @if ($errors->has('rol'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('rol') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <small for="nombre" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Nombre(s)</small>
                            <input type="text" class="form-control form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre') }}" autofocus>
                            @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-4">
                            <small for="apellidoPaterno" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Apellido Paterno</small>
                            <input type="text" class="form-control form-control{{ $errors->has('apellidoPaterno') ? ' is-invalid' : '' }}" id="apellidoPaterno" name="apellidoPaterno" value="{{ old('apellidoPaterno') }}">
                            @if ($errors->has('apellidoPaterno'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellidoPaterno') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-4">
                            <small for="apellidoMaterno" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Apellido Materno</small>
                            <input type="text" class="form-control form-control{{ $errors->has('apellidoMaterno') ? ' is-invalid' : '' }}" id="apellidoMaterno" name="apellidoMaterno" value="{{ old('apellidoMaterno') }}">
                            @if ($errors->has('apellidoMaterno'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellidoMaterno') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <small for="email">Correo electrónico</small>
                            <input type="email" class="form-control form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            <small for="telefono">Teléfono</small>
                            <input type="text" class="form-control form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" id="telefono" name="telefono" value="{{ old('telefono') }}">
                            @if ($errors->has('telefono'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <small for="curp">CURP</small>
                            <input type="text" class="form-control form-control{{ $errors->has('curp') ? ' is-invalid' : '' }}" id="curp" name="curp" value="{{ old('curp') }}">
                            @if ($errors->has('curp'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('curp') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-4">
                            <small for="imss">Número de IMSS</small>
                            <input type="text" class="form-control form-control{{ $errors->has('imss') ? ' is-invalid' : '' }}" id="imss" name="imss" value="{{ old('imss') }}">
                            @if ($errors->has('imss'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('imss') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-md-4">
                            <small for="nacimiento">Fecha de nacimiento</small>
                            <input type="date" timezone="America/Mexico_City" class="form-control form-control{{ $errors->has('nacimiento') ? ' is-invalid' : '' }}" id="nacimiento" name="nacimiento" value="{{ old('nacimiento') }}">
                            @if ($errors->has('nacimiento'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nacimiento') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md mt-4">
                            <input type="file" class="custom-file-input" id="identificacion" name="identificacion" lang="es">
                            <label class="custom-file-label" for="identificacion">Identificación</label>
                            <small>
                                @if($usuario->identificacion == NULL)
                                <span class="text-danger">Pendiente</span>
                                @else
                                <a href="{{Storage::url($usuario->identificacion)}}">Descargar</a>
                                @endif
                            </small>
                        </div>
                        <div class="form-group col-md mt-4">
                            <input type="file" class="custom-file-input" id="identificacion2" name="identificacion2" lang="es">
                            <label class="custom-file-label" for="identificacion2">Identificación 2</label>
                            <small>
                                @if($usuario->identificacion2 == NULL)
                                <span class="text-danger">Pendiente</span>
                                @else
                                <a href="{{Storage::url($usuario->identificacion2)}}">Descargar</a>
                                @endif
                            </small>
                        </div>
                        <div class="form-group col-md mt-4">
                            <input type="file" class="custom-file-input" id="domicilio" name="domicilio" lang="es">
                            <label class="custom-file-label" for="domicilio">Domicilio</label>
                            <small>
                                @if($usuario->domicilio == NULL)
                                <span class="text-danger">Pendiente</span>
                                @else
                                <a href="{{Storage::url($usuario->domicilio)}}">Descargar</a>
                                @endif
                            </small>
                        </div>
                        <div class="form-group col-md mt-4">
                            <input type="file" class="custom-file-input" id="nss" name="nss" lang="es">
                            <label class="custom-file-label" for="nss">NSS</label>
                            <small>
                                @if($usuario->nss == NULL)
                                <span class="text-danger">Pendiente</span>
                                @else
                                <a href="{{Storage::url($usuario->nss)}}">Descargar</a>
                                @endif
                            </small>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group">
                            <label>Plan</label>
                            <select name="plan_id" class="custom-select">
                                @foreach ($planes as $p)
                                    <option value="{{$p->id}}" 
                                        @if ($usuario->plan_id == $p->id)
                                            selected
                                        @endif
                                        >{{"Inscripción: $$p->sdi | Mensualidad: $$p->mensualidad"}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary my-3">
                        Guardar
                    </button>
                    <a href="#" onclick="window.history.back()" class="btn btn-secondary">Regresar</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection