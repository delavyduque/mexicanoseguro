<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>

    <table class="wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
        <tr>
            <td align="center">
                <table class="content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                    {{ $header ?? '' }}

                    <!-- Email Body -->
                    <tr>
                        <td class="body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                                <!-- Body content -->
                                <tr>
                                    <td class="content-cell">
                                        {{ Illuminate\Mail\Markdown::parse($slot) }}

                                        {{ $subcopy ?? '' }}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    {{ $footer ?? '' }}
                </table>
            </td>
        </tr>
    </table>
    <div class="subcopy">
        <p>Favor de no contestar este correo ya que es generado de forma automática por nuestros sistemas de cómputo.</p>
        <p>Centro de Atención a Clientes</p>
        <p>Este correo electrónico es confidencial, está legalmente protegido y/o puede contener información privilegiada. Si usted no es su destinatario o no es alguna persona autorizada para recibir sus correos electrónicos, NO deberá usted utilizar, copiar, revelar, o tomar ninguna acción basada en este correo electrónico o cualquier otra información incluida en él (incluyendo todos los documentos adjuntos).</p>
        <p>Si lo ha recibido por error, por favor notifique al <a href="mailto:admon@mexicanoseguro.com">emisor</a> e inmediatamente bórrelo de forma permanente y destruya cualquier copia impresa. En caso de que el correo esté dirigido a alguno de nuestros clientes, la opinión o recomendación contenida está sujeta a las condiciones regulatorias de OCSI Soluciones que resulten aplicables o a los acuerdos comerciales suscritos con el cliente.</p>
        <p>This e-mail is confidential, it may be legally protected and/or may contain privileged information. If you are not the addressee or authorized to receive this for the addressee, you must NOT use, copy, disclose, or take any action based on this message or any other information herein (including attachments). If you have received this e-mail in error, please notify the sender and immediately and permanently delete it and destroy any copies of it that were printed out. When addressed to our clients any opinions or advice contained in this Internet e-mail is subject to the terms and conditions expressed in any applicable governing OCSI Soluciones terms of business or client engagement letter.</p>
    </div>
</body>
</html>
