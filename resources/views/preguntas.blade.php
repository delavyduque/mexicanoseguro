@extends('layouts.app')

@section('styles')
<style>
.carousel-item {
	height: 85vh;
}

.carousel-item img {
    position: absolute;
    object-fit:cover;
    top: 0;
    left: 0;
    min-height: 85vh;
	width: 100%;
}

#fixa{
	position: absolute;
	z-index: 10;
	bottom: 2rem;
	left: 50%;
	/* bring your own prefixes */
	transform: translate(-50%, 0);
}



</style>
@endsection

@section('scripts')
<script>
$(function() {
	$('a').each(function() {
		if (!$( this ).hasClass( "original" ) && $(this).attr('href').includes("#pregunta")) {
			var pregunta = $(this).attr('href');
			pregunta = pregunta.substring(1, pregunta.length);
			var contenido = $(this).html();
			var nueva = `<a href="#${pregunta}" class="original" data-toggle="collapse" aria-expanded="false" aria-controls="${pregunta}" style="color: #b38e5d;">${contenido}</a>`;
			$(this).after(nueva);
		}
	});
	$('a').each(function() {
		if (!$( this ).hasClass( "original" ) && $(this).attr('href').includes("#pregunta")) {
			$(this).remove();
		}
	});
});
</script>
@endsection


@section('content')
	<!-- Page Heading -->
	<!-- Promo -->
	<!-- Preguntas frecuentes -->
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-centered">
				<h1> Preguntas frecuentes</h1>
			</div>
		<div class="col-md-12">
			<div class="card shadow mb-4">
				<div class="card-header py-3" style="background-color: #9d2449;">
					<h6 class="m-0 font-weight-bold" style="color: white;">Preguntas frecuentes</h6>
				</div>
				<div class="card-body">
					@foreach (App\Pregunta::all() as $p)
					<h4><a href="#pregunta{{$p->id}}" class="original" data-toggle="collapse" aria-expanded="false" aria-controls="pregunta{{$p->id}}" style="color: #b38e5d;">
						{{ $p->pregunta }} <span class="small"><i class="fas fa-chevron-down"></i></span>
					</a></h4>
					<div class="collapse" id="pregunta{{$p->id}}">
						{!! $p->respuesta !!}
					</div>
					<hr>
					@endforeach					
				</div> 
			</div> 
		</div>
	</div>
	
@endsection