<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ env('APP_NAME') }}</title>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src='/js/jquery-ui.min.js'></script>
	<!--Floating WhatsApp javascript-->
	<script type="text/javascript" src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.js"></script>
    <script>
		$(function() {
			$('[data-toggle="tooltip"]').tooltip();
			$('.toast').toast('show');

			$('#WAButton').floatingWhatsApp({
				phone: '{{env('WHATS')}}', //WhatsApp Business phone number International format-
				//Get it with Toky at https://toky.co/en/features/whatsapp.
				headerTitle: 'Chatea con nosotros', //Popup Title
				popupMessage: 'Hola, ¿cómo podemos ayudarte?', //Popup Message
				showPopup: true, //Enables popup display
				buttonImage: '<img src="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/whatsapp.svg" />', //Button Image
				//headerColor: 'crimson', //Custom header color
				//backgroundColor: 'crimson', //Custom background button color
				position: "right"    
			});
		});
    </script>
    @yield('scripts')

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
	<!--Floating WhatsApp css-->
	<link rel="stylesheet" href="https://rawcdn.githack.com/rafaelbotazini/floating-whatsapp/3d18b26d5c7d430a1ab0b664f8ca6b69014aed68/floating-wpp.min.css">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    @yield('styles')
</head>
<body>
	<nav id="page-top" class="navbar navbar-expand-lg navbar-dark mb-4 shadow" style="background-color: #13322b; background-image: linear-gradient(to right, #13322b , #596F6A);">
		<a class="navbar-brand text-white text-uppercase" href="/">{{env('APP_NAME')}}</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto text-white">
				@guest
				<li class="nav-item">
        			<a class="nav-link" href="/#definicion">Qué ofrecemos</a>
      			</li>
				<li class="nav-item dropdown">
        			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          			Beneficios
        			</a>
        			<div class="dropdown-menu" aria-labelledby="navbarDropdown">
          				<a class="dropdown-item" href="/#imss">IMSS</a>
						<a class="dropdown-item" href="/#infonavit">Infonavit</a>
						<a class="dropdown-item" href="#">Oferta</a>
						<a class="dropdown-item" href="/#testimonios">Testimonios</a>  
        			</div>
				</li>
				<li class="nav-item">
        			<a class="nav-link" href="/#oferta">Nuestra oferta</a>
				</li>
				<li class="nav-item">
        			<a class="nav-link" href="/preguntas">Preguntas comunes</a>
				</li>
				<li class="nav-item">
        			<a class="nav-link" href="/#contacto">Contáctanos</a>
				  </li>
				<li class="nav-item">
					<a class="nav-link text-white" href="{{ route('login') }}">{{ __('Inicia sesión') }}</a>
				</li>
				<li class="nav-item mr-2 text-center">
					<h5><i class="fas fa-phone-alt"></i> Ventas</h5>
					<h6>{{setting('tel_ventas')}}</h6>
				</li>
				@else

					@if (Auth::user()->role_id == 1)
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="administracion" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  Administracion
						</a>
						<div class="dropdown-menu" aria-labelledby="administracion">
						  	<a class="dropdown-item" href="/settings">Plataforma</a>
						  	<a class="dropdown-item" href="/sueldo">Promociones</a>
							<a class="dropdown-item" href="/pregunta">Preguntas</a>
							<a class="dropdown-item" href="/comentario">Comentarios</a>
						</div>
					</li>
					@endif
		
					@if(Auth::user()->role_id <= 2)
					<li class="nav-item">
						<a class="nav-link" href="/usuario">Usuarios</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/cliente">Clientes</a>
					</li>
					@endif

					@if(Auth::user()->role_id <= 3)
					<li class="nav-item">
						<a class="nav-link" href="/venta">Ventas</a>
					</li>
					@endif

					<li class="nav-item">
						<a class="nav-link" href="{{ route('logout') }}" 
							onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
						{{ __('Cierra sesión') }}</a>
					</li>
				@endguest 
			</ul>
		</div>
	</nav>
	<div class="container-fluid">
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			@csrf
		</form>
		
		@if (Session::has('mensaje'))
		<div class="alert alert-primary text-center" role="alert">
			{{session('mensaje')}}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif

		@yield('content')

		<div id="WAButton"></div>
	</div>
</body>
</html>
