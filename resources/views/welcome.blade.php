@extends('layouts.app')

@section('styles')
<style>
	.carousel-item.active.test,{
		display:block;
	}

	#fixa{
		position: absolute;
		z-index: 10;
		bottom: 5%;
		left: 50%;
		/* bring your own prefixes */
		transform: translate(-50%, 0);
	}
	
</style>
@endsection

@section('scripts')
<script>
	$(function() {
		var els = $('figure.media');
		$(els).each(function() {
			var url = $(this).children().attr('url');
			url = url.substring(url.indexOf('?v=') + 3);
			var t = '<center><iframe width="560" height="315" src="https://www.youtube.com/embed/' + url + '" frameborder="0" allowfullscreen></iframe></center>';
			$(this).before(t);
			$(this).remove();
		});
		$('#mostrarRegistro').click(function(){
			$('#modalRegistro').modal('show');
		});
		$('#contrato').click(function(){
			$('#register').unbind('submit');
			$('#register').submit();
		});

		$('#register').submit(function(e){
			e.preventDefault();
			$('#nombreCliente').html(
				$('#nombre').val()
			);
			$('#modalContrato').modal('show');
		});
	});
</script>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<!-- Page Heading -->
	<!-- Promo -->
<section id="promo">
	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" style="max-height: 85vh">
			<div id="fixa">
				<div class="text-center">
					<a href="#contacto" class="btn btn-primary btn-lg">Contáctanos</a>
				</div>
			</div>
			<div class="carousel-item active">
				<img class="d-block w-100" src="{{setting('front1') != null ? env('APP_URL') . "/storage/" . setting('front1') : 'images/banner_infonavit.jpg'}}" alt="First slide" class="img-fluid">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="{{setting('front2') != null ? env('APP_URL') . "/storage/" . setting('front2') : 'images/banner_pension.jpg'}}" alt="Second slide" class="img-fluid">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="{{setting('front3') != null ? env('APP_URL') . "/storage/" . setting('front3') : 'images/promo3.jpg'}}" alt="Third slide" class="img-fluid">
			</div>
			{{-- 
			<div class="carousel-item">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/TTx_2mevUrQ" allowfullscreen></iframe>
				</div>
			</div>
			--}}
		</div>
		<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</section>
	
	<!-- Fin Promo -->
	
	<!-- Definición-->
<section id="definicion" class="pt-5">
	<div class="jumbotron jumbotron-fluid">
  		<div class="container">
		  	<div class="row">
				<div class="col-md-4">
					@if (setting('desc') != null)
						<img src="{{env('APP_URL') . "/storage/" . setting('desc')}}" class="img-fluid" alt="Responsive image">
					@else
						<img src="/images/side1.jpg" class="img-fluid" alt="Responsive image">
					@endif
				</div>
				<div class="col-md-8">
					<h1>¿Qué es {{env('APP_NAME')}}?</h1>
					<span class="font-weight-bold" style="font-size: 1.1em">
						{!! setting('description', $lorem) !!}
					</span>
					<div class="text-center">
						<a href="#contacto" class="btn btn-primary btn-lg">Contáctanos</a>
					</div>
				</div> 
			</div>
  		</div>
	</div>			
</section>
<!-- Fin definición-->

<!-- Características -->
<section id="caracteristicas">
	<div class="row pt-5">
		<div class="col-md-12 text-center"> 
			<h1>Beneficios</h1>
		</div>
	</div>
	<section id="imss">
		<div class="jumbotron jumbotron-fluid ">
  			<div class="container">
		  		<div class="row">
					<div class="col-md-5">
						@if (setting('imss_img') != null)
							<img src="{{env('APP_URL') . "/storage/" . setting('imss_img')}}" class="img-fluid" alt="Responsive image">
						@else
							<img src="/images/imss.jpg" class="img-fluid" alt="Responsive image">
						@endif

						@if (setting('imss_img2') != null)
							<img src="{{env('APP_URL') . "/storage/" . setting('imss_img2')}}" class="img-fluid mt-2" alt="Responsive image">
						@else
							<img src="/images/imss_logo.jpg" class="img-fluid mt-2" alt="Responsive image">
						@endif

					</div>
					<div class="col-md-7">
						<h1>IMSS</h1>
						<span class="font-weight-bold" style="font-size: 1.1em">
							{!! setting('imss', $lorem) !!}
						</span>
						<div class="text-center">
							<a href="#contacto" class="btn btn-primary btn-lg">Contáctanos</a>
						</div>
					</div> 
				</div>
  			</div>
		</div>
	</section>
	<section id="infonavit">
		<div class="jumbotron jumbotron-fluid ">
  			<div class="container">
		  		<div class="row">
					<div class="col-md-7">
						<h1>Infonavit</h1>
						<span class="font-weight-bold" style="font-size: 1.1em">
							{!! setting('infonavit', $lorem) !!}
						</span>
					</div> 
					<div class="col-md-5 align-self-center">
						@if (setting('infonavit_img') != null)
							<img src="{{env('APP_URL') . "/storage/" . setting('infonavit_img')}}" class="img-fluid" alt="Responsive image">
						@else
							<img src="/images/infonavit.jpg" class="img-fluid" alt="Responsive image">
						@endif
					</div>
				</div>
				<div class="text-center mt-3">
					<a href="#contacto" class="btn btn-primary btn-lg">Contáctanos</a>
				</div>
  			</div>
		</div>
	</section>
	<section id="oferta">
		<div class="jumbotron jumbotron-fluid ">
  			<div class="container">
		  		<div class="row">
				  	<div class="col-md-5">
						@if (setting('pension_img') != null)
							<img src="{{env('APP_URL') . "/storage/" . setting('pension_img')}}" class="img-fluid" alt="Responsive image">
						@else
							<img src="/images/pension1.jpg" class="img-fluid" alt="Responsive image">
						@endif
					</div>
					<div class="col-md-7">
						<h1>Oferta</h1>
						<span class="font-weight-bold" style="font-size: 1.1em">
							{!! setting('oferta', $lorem) !!}
						</span>
						<div class="text-center">
							<a href="#contacto" class="btn btn-primary btn-lg">Contáctanos</a>
						</div>
					</div> 
				</div>
  			</div>
		</div>
	</section>
</section>
	<!-- Fin Características -->

<!-- Precio -->
<section id="oferta">
	<div class="card shadow my-4" style="background-color: #13322b; background-image: linear-gradient(to right, #13322b , #9d2449);">
		<div class="row container">
			<div class="col-md-6 text-center my-5">
				<h1 class="text-center" style="color: #fff">Por solo</h1>
				<h1 class="font-weight-bold display-3" style="color: #fff">
					${{substr( number_format($plan->mensualidad, 2, '.', ''), 0, strpos ( number_format($plan->mensualidad, 2, '.', ''), '.') + 1)}}<sup><small style="font-size:2rem">{{strpos ( number_format($plan->mensualidad, 2, '.', ''), '.') !== false ? substr( number_format($plan->mensualidad, 2, '.', ''), strpos ( number_format($plan->mensualidad, 2, '.', ''), '.') + 1) : ''}}</small></sup> <small>{{$currency}}</small>
				</h1>
				<small class="lead m-0" style="color: #fff">al mes</small>
			</div>
			<div class="col-md-6 my-5 container d-flex align-items-center">
				<div>
					<h4 style="color: #fff"><i class="fas fa-check-square"></i> <b>Cuota única</b> de inscripción
					${{substr( number_format($plan->sdi, 2, '.', ''), 0, strpos ( number_format($plan->sdi, 2, '.', ''), '.') + 1)}}<sup><small style="font-size:0.55rem">{{strpos ( number_format($plan->sdi, 2, '.', ''), '.') !== false ? substr( number_format($plan->sdi, 2, '.', ''), strpos ( number_format($plan->sdi, 2, '.', ''), '.') + 1) : ''}}</small></sup> <small>{{$currency}}</small>
					</h4>
					<h4 style="color: #fff"><i class="fas fa-check-square"></i> <b>Protege</b> a tu familia</h4>
					<h4 style="color: #fff"><i class="fas fa-check-square"></i> <b>Sin trámites</b> largos ni plazos forzosos</h4>
					<h4 style="color: #fff"><i class="fas fa-check-square"></i> <b>Rápida y eficiente</b> atención a clientes</h4>
					<a href="#promo" class="btn btn-primary btn-lg">Contáctanos</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Fin Precio -->

@if (App\Comentario::all()->count() > 0)
<div id="carouselContent" class="carousel slide" data-ride="carousel" style="background-color: #13322b; background-image: linear-gradient(to right, #13322b , #596F6A);" >
	<h1 class="text-white text-center" style="background-color: #9d2449">Testimonios</h1>
	<ol class="carousel-indicators position-relative">
		@php
			$comentarios = App\Comentario::all();
		@endphp
		@for ($i = 0; $i < $comentarios->count(); $i++)
			<li data-target="#carouselContent" data-slide-to="{{ $i }}" class="{{$i == 0 ? 'active' : ''}}"></li>
		@endfor
	</ol>
	<div class="container">
		<div class="carousel-inner" role="listbox">
			@for ($i = 0; $i < $comentarios->count(); $i++)
				<div class="carousel-item {{$i == 0 ? 'active' : ''}} test text-center p-4">
					<div class="row">
						<div class="col-sm-3">
							@if ($comentarios[$i]->imagen)
								<img src="/storage/{{ $comentarios[$i]->imagen }}" alt="" class="img-fluid mb-2" style="min-height: 8rem; height:8rem; width:8rem; position:relative">
							@else
								<img src="/images/imagen_cliente.png" alt="" class="img-fluid mb-2" style="min-height: 8rem; height:8rem; width:8rem; position:relative">
							@endif
						</div>
						<div class="col-sm-9">
							<p class="text-white"><i>"{{ $comentarios[$i]->contenido }}"</i></p>
							<small class="text-white">{{ $comentarios[$i]->nombre_cliente }}</small>
						</div>
					</div>
				</div>
			@endfor
		</div>
	</div>
</div>
@endif


@guest
<section id="contacto" class="my-5">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<div class="card shadow mb-4" id="contactanos">
				<div class="card-header py-3" style="background-color: #9d2449;">
					<h5 class="m-0 font-weight-bold text-center" style ="color: #fff">Contáctanos</h5>
				</div>
				<div class="card-body justify-content-center">
					<form method="POST" action="/contacto" class="container" id="contact_form">
						@csrf
						<div class="row">
							<div class="form-group col-6">
								<label for="contactName">Nombre:</label>
								<input type="text" class="form-control" name="nombre" placeholder="Nombre Apellidos" required>
							</div>
							<div class="form-group col-6">
								<label for="contactEmail">Correo electrónico:</label>
								<input type="email" class="form-control" name="correo" placeholder="correo@correo.com" required>
							</div>
						</div>
						<div class="form-group">
							<label for="contactSubject">Asunto:</label>
							<input type="text" class="form-control" asunto="asunto" placeholder="Asunto del correo">
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Mensaje</label>
							<textarea class="form-control" name="mensaje" rows="4" placeholder="Escribe aquí tu mensaje" form="contact_form" required></textarea>
						</div>
						<button type="submit" class="btn btn-primary mt-3 text-white justify-content-center">
							{{ __('Enviar') }}
						</button>
					</form>
				</div>
			</div>
		</div> 
		</div>
	</div>
</section>	
@endguest
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer d-flex justify-content-center" style="background-color: #13322b; background-image: linear-gradient(to right, #13322b , #596F6A);">
	<div class="row container my-auto">
		<div class="col-md-4 text-center my-auto">
			<p style="color: #fff;">{{ env('APP_NAME')}} &copy; 2019</p>
			<p style="color: #fff;"><a href="#" style="color: #fff" data-toggle="modal" data-target="#privacidad">Política de Privacidad</a></p>
			<p style="color: #fff;"><a href="#" style="color: #fff" data-toggle="modal" data-target="#condiciones">Términos y condiciones</a></p>
		</div>
		<div class="col-md-4 text-center my-auto">
			<p style="color: #fff;">Para más informes llama al <br> {{ setting('tel_informes') }}</p>
			<p style="color: #fff;">Escríbenos tus dudas al correo: informes@mexicanoseguro.com</p>
		</div>
		<div class="col-md-4 text-center my-auto">
			<p style="color: #fff;">Dirección:</p>
			<p class="text-white">{{setting('line1')}}</p>
			<p class="text-white">{{setting('line2')}}</p>
		</div>
	</div>
</footer>
<!-- End of Footer -->

<!-- Modal -->
<div id="privacidad" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				AVISO DE PRIVACIDAD
			</div>
			<div class="modal-body">
				<p>Como consecuencia de la relación contractual entre las partes y para la ejecución del presente Contrato, las partes podrán recibir de la otra parte, en su carácter de responsables en términos de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares (la “LFPDPPP”) y su respectivo Reglamento, datos de carácter personal (los “Datos Personales”) regulados por la LFPDPPP y legislación secundaria en materia de protección de datos personales.</p>
				
				<p>Para tales efectos, las partes se obligan, respecto de los Datos Personales, a:</p>
				
				<ol>
					<li>Utilizar y tratar los Datos Personales con el único y exclusivo objeto de cumplir con el Contrato y siguiendo en todo caso las instrucciones recibidas del responsable de los Datos Personales, según se define en la LFPDPPP. Las partes expresamente se abstendrán de dar a los Datos Personales cualquier uso distinto al acordado en el presente Contrato y, en especial, se abstendrán de alterarlos, utilizarlos para su propio interés o comunicarlos o permitir el acceso de terceros a los mismos, así como utilizarlos para fines distintos para los cuales son transferidos.</li>
					<li>A conservar confidencialidad respecto de los Datos Personales que les sean transferidos, comprometiéndose a no revelar a ninguna tercera persona alguno de estos Datos Personales.</li>
					<li>Devolver al responsable una vez concluida la relación contractual por virtud del presente Contrato, todos los documentos y archivos en los que se encuentren almacenados todos o alguno de los Datos Personales transferidos por el Responsable, cualquiera que sea su soporte o formato, así como las copias de los mismos. Asimismo, las partes deberán eliminar de sus bases de datos todos los Datos Personales que hayan sido transferidos por virtud del presente Contrato dentro de los 5 (cinco) días siguientes a la terminación de la vigencia del presente Contrato.</li>
					<li>Restringir a los empleados, agentes y colaboradores el acceso y el uso de los Datos Personales a aquellos que sean absolutamente imprescindibles para el desarrollo del objeto del Contrato, obligándose los Responsables a imponer a los mismos las obligaciones de confidencialidad y de prohibición de uso respecto de los Datos Personales, en los mismos términos que se prevén en la presente cláusula y comprometiéndose a responder de cualquier incumplimiento de las referidas obligaciones por parte de cualesquiera de sus empleados, agentes y colaboradores anteriormente citados.</li>
					<li>No transferir los Datos Personales sin autorización del responsable de la protección de datos personales que cada una de las partes haya designado dentro de su organización. Siempre que dicho responsable autorice una transferencia de Datos Personales, la parte autorizada deberá asegurarse de que el tercero que reciba los datos personales cumpla con lo establecido en la presente cláusula para la protección de los Datos Personales.</li>
					<li>Adoptar las medidas de seguridad de protección de los Datos Personales que sean proporcionadas por “LAS PARTES”, (en adelante las “Medidas de Seguridad”), así como ir actualizando dichas Medidas de Seguridad conforme a la regulación vigente durante la vigencia del Contrato y cualesquiera otras que sean notificadas por las partes.</li>
					<li>Indemnizar a la parte perjudicada por todos aquellos gastos, daños y perjuicios que le ocasione el incumplimiento de cualesquiera de las obligaciones previstas en la presente cláusula, estando explícitamente incluidos en estos daños y perjuicios cualquier multa y/o sanción que pudieran imponer las autoridades correspondientes al Responsable como consecuencia de un eventual incumplimiento de la presente cláusula, así como las demandas que pudieran promover los titulares de los datos personales, siempre que sea demostrado ante las autoridades correspondientes.</li>
					<li>“LAS PARTES” o su representante legal debidamente acreditado podrán limitar el uso o divulgación de sus datos personales; así como ejercer los derechos de Acceso, Rectificación, Cancelación u Oposición al tratamiento de los datos que la Ley prevé (los “Derechos ARCO”) mediante los formatos que  la parte responsable pondrá a su disposición.</li>
				</ol>
			</div>

	  </div>
	</div>
</div>

<div id="condiciones" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
	  <div class="modal-content">
		<div class="modal-header">
			Términos y Condiciones de Uso
		</div>
		<div class="modal-body">	
			<p>Es requisito necesario para la adquisición de los productos que se ofrecen en este sitio, que lea y acepte los siguientes Términos y Condiciones que a continuación se redactan. El uso de nuestros servicios, así como la compra de nuestros productos implicará que usted ha leído y aceptado los Términos y Condiciones de Uso en el presente documento. Todos los productos y/ tramites que son ofrecidos por nuestro sitio web. En algunos casos, para adquirir un producto, será necesario el registro por parte del usuario, con ingreso de datos personales fidedignos y definición de una contraseña.</p>
			<p>El usuario puede elegir y cambiar la clave para su acceso de administración de la cuenta en cualquier momento, en caso de que se haya registrado y que sea necesario para la compra de alguno de nuestros productos.  no asume la responsabilidad en caso de que entregue dicha clave a terceros.</p>
			<p>Todas las compras y transacciones de nuestros tramites que se lleven a cabo por medio de este sitio web, están sujetas a un proceso de confirmación y verificación, validación de la forma de pago, validación de la factura (en caso de existir) y el cumplimiento de las condiciones requeridas por el medio de pago seleccionado. En algunos casos puede que se requiera una verificación por medio de correo electrónico.</p>
			<p>Los precios de los productos ofrecidos en esta sitio web es válido solamente en las compras realizadas en este sitio web.</p>
			
			<h3>FORMA DE PAGO</h3>
			<p>Manifiestan ambas partes de conformidad que “EL CLIENTE” se obliga a pagar a “EL PRESTADOR” el importe correspondiente por concepto de “LOS SERVICIOS” objeto del presente, conforme a lo que a continuación se expone: Dicho pago se hará el día de cada mes que realizó su alta.</p>
			<p>En el entendido de que todas las transacciones con tarjeta serán procesadas a través de Openpay.</p>
			
			<h3>USO NO AUTORIZADO</h3>
			<p>En caso de que aplique la venta de nuestros productos y/o tramites, usted no puede colocar uno de nuestros productos, o tramites modificado o sin modificar, en un CD, sitio web o ningún otro medio y ofrecerlos para la redistribución o la reventa de ningún tipo.</p>
			
			<h3>PROPIEDAD</h3>
			<p>Usted no puede declarar propiedad intelectual o exclusiva a ninguno de nuestros productos, modificado o sin modificar. Todos los productos son propiedad  de los proveedores del contenido. En caso de que no se especifique lo contrario, nuestros productos se proporcionan  sin ningún tipo de garantía, expresa o implícita. En ningún esta compañía será  responsables de ningún daño incluyendo, pero no limitado a, daños directos, indirectos, especiales, fortuitos o consecuentes u otras pérdidas resultantes del uso o de la imposibilidad de utilizar nuestros productos.</p>
			<p>COMPROBACIÓN ANTIFRAUDE</p>
			<p>La compra del cliente puede ser aplazada para la comprobación antifraude. También puede ser suspendida por más tiempo para una investigación más rigurosa, para evitar transacciones fraudulentas.</p>
			
			<h3>PRIVACIDAD</h3>
			<p>Este sitio web  garantiza que la información personal que usted envía cuenta con la seguridad necesaria. Los datos ingresados por usuario o en el caso de requerir una validación de los pedidos no serán entregados a terceros, salvo que deba ser revelada en cumplimiento a una orden judicial o requerimientos legales.</p>
			
			<h3>CONFIDENCIALIDAD</h3>
			<p>En relación con los documentos e información que la Partes se entreguen o intercambiarán entre sí por diversos medios, ya sea de manera verbal o escrita, con motivo de las negociaciones, así como la que ha intercambiado, intercambiará y/o entregará durante la vigencia y ejecución del presente contrato, cuyo contenido desea proteger (en adelante, la “Información Confidencial”), Las Partes se obligan a mantener en estricto secreto y confidencialidad y no podrá informar o entregar, divulgar, o en cualquier forma revelar a ningún tercero, ni autoridad, cualquier Información Confidencial que reciba directa o indirectamente y por cualquier medio de Modelo en virtud del presente contrato o su ejecución.</p>
			
			<h3>AUSENCIA DE VICIOS</h3>
			<p>Las partes manifiestan que en la celebración del presente contrato no existe Error, Dolo, Mala Fe, Violencia, ni Lesiones, y que lo otorgan con plena capacidad Legal.</p>
			
			<h3>JURISDICCIÓN Y LEYES APLICABLES</h3>
			<p>Para la interpretación, cumplimiento y ejecución del presente Contrato “LAS PARTES” convienen sujetarse a la jurisdicción de los Tribunales en el Estado de Nuevo León, renunciando por ello a cualquier jurisdicción que por razón de domicilios presentes o futuros les pueda corresponder, siendo aplicables las Leyes vigentes en los Estados Unidos Mexicanos.</p>
		</div>
	  </div>
	</div>
</div>

<div class="modal fade" id="modalContrato" tabindex="-1" role="dialog" aria-labelledby="otraCosa" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="otraCosa">Contrato</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Contrato de Confidencialidad, que celebran, por una parte {{ env('APP_NAME')}} representada en este acto por Monserrat Mazatini, y a quien en lo sucesivo y por razones de brevedad se le denominará “LA EMPRESA” y, por la otra, por su propio derecho, <b><span id="nombreCliente"></span></b>, a quien en lo sucesivo se le denominará “EL COLABORADOR”, y LAS PARTES, para referirse tanto a LA EMPRESA como al Empleado, al tenor de las siguientes:</p>

					<p>DECLARACIONES</p>
					<p>I. DECLARA “LA EMPRESA”: </p>

					<p>a. Que ha desarrollado cierta información técnica, tecnológica, comercial, de fabricación y de negocios y cuenta con la organización necesaria para cumplir con sus objetivos técnicos, tecnológicos, industriales, administrativos, comerciales y de negocios, y que ocasionalmente obtiene, por la vía legal, cierta “Información” de terceros.</p>

					<p>II. DECLARA “EL COLABORADOR”: </p>

					<p>a. Que su nombre es XXXXXXXX ser ciudadano mexicano, mayor de edad y con capacidad suficiente para ejercer sus derechos y obligaciones;</p>

					<p>b. Que, como empleado de “LA EMPRESA”, está consciente de sus obligaciones de confidencialidad de los secretos técnicos, comerciales y de fabricación de los cuales tiene conocimiento por razón de su empleo, establecidos en el Artículo 134 Fracción XIII de la Ley Federal del Trabajo.</p>

					<p>c. Que está consciente que la violación de tales obligaciones de confidencialidad, configuran los delitos establecidos por el Articulo 223 Fracciones III y IV de la Ley de la Propiedad Industrial y por los Artículos 210, 211 y 211 bis del Código Penal para el Distrito Federal, y los correlativos de todos los Estados de la República Mexicana; así como las sanción establecida en el Artículo 224 de la Ley de la Propiedad Industrial, en los mismos Artículos 210, 211 y 212 bis del Código Penal para el Distrito Federal y en el Artículo 47 de la Ley Federal de Trabajo.</p>

					<p>III. Declaran LAS PARTES:</p>

					<p>a. Ambas partes reconocen mutuamente la personalidad que ostentan y con fundamento en las anteriores DECLARACIONES, expresan que es su libre voluntad celebrar en presente Contrato conforme a las siguientes:</p>

					<p>CLÁUSULAS</p>

					<p>PRIMERA: EL EMPLEADO reconoce que el uso y manejo de la información financiera, estratégica, ventas, planes de desarrollo de productos, políticas de precios, mercadotecnia, sistemas de distribución, proyectos, etc., a que tiene acceso con motivo del desempeño de su actividad, constituye para la empresa donde presta sus servicios, una ventaja competitiva frente a terceros, clientes, proveedores, o cualquier persona física o moral, etc., y por ende, es de carácter confidencial, y por ello, desde ahora, se obliga y compromete a no divulgarla o transmitirla, ni en forma verbal o escrita, ni por ningún otro medio a terceros, (persona física o moral), o unidades económicas, ya constituidas operando o no, o que estén en vías de operación o constitución, ni a utilizarla para fines distintos o ajenos al desarrollo de mi trabajo y actividades conexas que me encomienda la empresa, asumiendo si responsabilidad por los daños y perjuicios que resulten, independientemente de la responsabilidad penal en que incurra por el quebrantamiento del presente compromiso, haciéndose sabedor del contenido de los artículos 82,85,86,223, Fracc. IV, 224 y 226, de la Ley de Propiedad Industrial.</p>

					<p>SEGUNDA. La Información Confidencial es y será propiedad exclusiva de “LA EMPRESA” y, por tanto, EL EMPLEADO se obliga a utilizar la Información que reciba, exclusivamente para ejecutar su trabajo como empleado de “LA EMPRESA”  y su uso para cualquier otro fin queda plena y totalmente excluido y prohibido, con las consecuencias correspondientes que marca la legislación aplicable incluyendo la establecida en las declaraciones.</p>

					<p>TERCERA. En caso de incumplimiento de la confidencialidad y prohibición anteriormente establecidas, se le aplicara a “EL COLABORADOR” una PENA CONVENCIONAL de $250, 000.00 pesos (Doscientos Cincuenta Mil pesos m.n. 00/100) por los daños y perjuicios causados, independientemente de las sanciones corporales y pecuniarias a que se hará acreedor de conformidad con lo establecido por la Ley de Fomento y Protección de la Propiedad Industrial y su reglamento, así como los demás ordenamientos legales aplicables al caso.</p>

					<p>CUARTA. - La presente responsabilidad de “EL COLABORADOR” adquirida con este instrumento las partes acuerdan que será por la duración de la Relación Laboral, y se extiende hasta un termino no menor de 05 año después de haber terminado el vínculo laboral antes mencionado.</p>

				<p>QUINTA. - Para los efectos de interpretación y cumplimiento del presente Contrato, las Partes se someten expresamente a la Jurisdicción de los Tribunales competentes de la Monterrey, Nuevo Leon, renunciando a cualquier otro que les pudiera corresponder en razón de su domicilio presente o futuro.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Rechazar</button>
				<button type="submit" class="btn btn-primary"  id="contrato">Aceptar</button>
			</div>
		</div>
	</div>
</div>
@endsection