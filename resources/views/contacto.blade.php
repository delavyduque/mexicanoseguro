@extends('layouts.app')

@section('styles')
<style>
.carousel-item {
	height: 85vh;
}

.carousel-item img {
    position: absolute;
    object-fit:cover;
    top: 0;
    left: 0;
    min-height: 85vh;
	width: 100%;
}

#fixa{
	position: absolute;
	z-index: 10;
	bottom: 2rem;
	left: 50%;
	/* bring your own prefixes */
	transform: translate(-50%, 0);
}



</style>
@endsection

@section('scripts')
<script>
$(function() {
	$('#mostrarRegistro').click(function(){
		$('#modalRegistro').modal('show');
	});
	$('#mostrarContrato').click(function(){
		$('#nombreCliente').html(
			$('#nombre').val() + ' ' + 
			$('#apellidoPaterno').val() + ' ' + 
			$('#apellidoMaterno').val()
		);
		$('#modalContrato').modal('show');
	});
});
</script>
@endsection


@section('content')
	<!-- Page Heading -->
	<!-- Promo -->
	<!-- Preguntas frecuentes -->
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<div class="card shadow mb-4" id="contactanos">
				<div class="card-header py-3" style="background-color: #9d2449;">
					<h5 class="m-0 font-weight-bold text-center" style ="color: #fff">Contáctanos</h5>
				</div>
				<div class="card-body justify-content-center">
					<form method="POST" action="/contacto" class="container" enctype="" id="contacto">
						@csrf
						<div class="row">
							<div class="form-group col-6">
								<label for="contactName">Nombre:</label>
								<input type="text" class="form-control" name="nombre" placeholder="Nombre Apellidos" required>
							</div>
							<div class="form-group col-6">
								<label for="contactEmail">Correo electrónico:</label>
								<input type="email" class="form-control" name="correo" placeholder="correo@correo.com" required>
							</div>
						</div>
						<div class="form-group">
							<label for="contactSubject">Asunto:</label>
							<input type="text" class="form-control" asunto="asunto" placeholder="Asunto del correo">
						</div>
						<div class="form-group">
							<label for="exampleFormControlTextarea1">Mensaje</label>
							<textarea class="form-control" name="mensaje" rows="4" placeholder="Escribe aquí tu mensaje" form="contacto" required></textarea>
						</div>
						<button class="btn btn-primary mt-3 text-white justify-content-center">
							{{ __('Enviar') }}
						</button>
					</form>
				</div>
			</div>
		</div> 
		</div>
	</div>
	
@endsection