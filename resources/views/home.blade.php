@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script src="https://www.paypal.com/sdk/js?client-id={{ env('PAYPAL_ID') }}&currency={{$currency}}&disable-funding=card">
</script>
<script src="https://js.stripe.com/v3/"></script>
<script>
$(function() {
    clientSecret = '{{$client_secret}}';


    stripe = Stripe('{{ env('STRIPE_PUPLIC') }}');
    var elements = stripe.elements();

    var style = {
        base: {
            color: "#32325d",
        }
    };

    var card = elements.create("card", { style: style });
    card.mount("#card-element");

    card.on('change', ({error}) => {
        const displayError = document.getElementById('card-errors');
        if (error) {
            displayError.textContent = error.message;
        } else {
            displayError.textContent = '';
        }
    });

    var form = document.getElementById('payment-form');

    form.addEventListener('submit', function(ev) {
        ev.preventDefault();
        stripe.confirmCardPayment(clientSecret, {
            payment_method: {
                card: card,
                billing_details: {
                    name: '{{Auth::user()->nombre}}'
                }
            }
        }).then(function(result) {
            if (result.error) {
            // Show error to your customer (e.g., insufficient funds)
            $('#error').html(result.error.message).show();
            } else {
            // The payment has been processed!
            if (result.paymentIntent.status === 'succeeded') {
                // Show a success message to your customer
                // There's a risk of the customer closing the window before callback
                // execution. Set up a webhook or plugin to listen for the
                // payment_intent.succeeded event that handles any business critical
                // post-payment actions.
                $('#pago').submit();
            }
            }
        });
    });

    
    paypal.Buttons({
        createOrder: function(data, actions) {
            // This function sets up the details of the transaction, including the amount and line item details.
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '{{Auth::user()->subscripciones->where("cantidad", '>=', 0)->last() == null ? $plan->mensualidad + $plan->sdi : $plan->mensualidad}}'
                    }
                }]
            });
        },
        onApprove: function(data, actions) {
            // This function captures the funds from the transaction.
            return actions.order.capture().then(function(details) {
                // This function shows a transaction success message to your buyer.
                $('#pago').submit();
            });
        }
    }).render('#paypal-button-container');

    $('#dataTable').DataTable({
        'iDisplayLength': 5,
        'dom': 'Bfrtip',
        'language' : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ mensualidades",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "No hay mensualidades registradas",
            "sInfo":           "Mostrando mensualidades de la _START_ a la _END_ de un total de _TOTAL_ mensualidades",
            "sInfoEmpty":      "Mostrando mensualidades de la 0 a la 0 de un total de 0 mensualidades",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ mensualidades)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
});
</script>
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h1 class="h3 mb-4 text-gray-800">Hola {{Auth::user()->nombre}}</h1>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold">Información de pago</h6>
                </div>
                <div class="card-body overflow-auto">
                    <div class="row">
                        <div class="col-md-6 text-center my-5">
                            <h1 class="text-center">Por solo</h1>
                            <h1 class="font-weight-bold display-3">
                                @if (strpos ($plan->mensualidad, '.') !== false)
                                ${{substr((string)$plan->mensualidad, 0, strpos ((string)$plan->mensualidad, '.') + 1)}}<sup><small style="font-size:2rem">{{strpos ((string)$plan->mensualidad, '.') !== false ? substr($plan->mensualidad, strpos ($plan->mensualidad, '.') + 1) : ''}}</small></sup> <small>{{$currency}}</small>
                                @else
                                ${{$plan->mensualidad}} <small>{{$currency}}</small>
                                @endif
                            </h1>
                            <small class="lead m-0">al mes</small>
                        </div>
                        <div class="col-md-6 my-5 container d-flex align-items-center">
                            <div>
                                <h4><i class="fas fa-check-square"></i> <b>Cuota única</b> de inscripción
                                @if (strpos ($plan->sdi, '.') !== false)
                                ${{substr($plan->sdi, 0, strpos ($plan->sdi, '.') + 1)}}<sup><small style="font-size:0.6rem">{{strpos ($plan->sdi, '.') !== false ? substr($plan->sdi, strpos ($plan->sdi, '.') + 1) : ''}}</small></sup> <small>{{$currency}}</small>
                                @else
                                ${{$plan->sdi}} <small>{{$currency}}</small>
                                @endif
                                </h4>
                                <h4><i class="fas fa-check-square"></i> <b>Protege</b> a tu familia</h4>
                                <h4><i class="fas fa-check-square"></i> <b>Sin trámites</b> largos ni plazos forzosos</h4>
                                <h4><i class="fas fa-check-square"></i> <b>Rápida y eficiente</b> atención a clientes</h4>
                            </div>
                        </div>
                    </div>
                    <div id="error" class="alert alert-warning text-center" role="alert" style="display: none">
                    </div>
                    @if(Auth::user()->subscripciones->where('billing_id', '!=', null)->count() > 0)
                        <button class="btn btn-success btn-block mt-2 text-uppercase" disabled>Contratado</button>
                    @elseif(Auth::user()->subscripciones->where('billing_id', null)->last() != null && Carbon\Carbon::parse(Auth::user()->subscripciones->where('plan_id', null)->last()->hasta) > Carbon\Carbon::now())
                        <button class="btn btn-success btn-block mt-2 text-uppercase" disabled>Contratado hasta {{Carbon\Carbon::parse(Auth::user()->subscripciones->where('plan_id', null)->last()->hasta)->isoFormat('DD MMM YYYY')}}</button>
                        <div class="mx-auto mt-3">
                            <form id="payment-form">
                                <div id="card-element">
                                    <!-- Elements will create input elements here -->
                                </div>

                                <!-- We'll put the error messages in this element -->
                                <div id="card-errors" role="alert"></div>

                                <button id="submit" class="btn btn-primary my-2 btn-block">Pagar</button>
                            </form>
                            <form action="/paypal/contrato" method="POST" id="pago">
                                @csrf
                                {{-- Para domiciliar <button class="btn btn-primary mt-2 btn-block" style="background-color: #000; border-color: #000" type="submit">Domiciliar pago</button> --}}
                            </form> 
                            <div id="paypal-button-container"></div>
                        </div>
                    @else
                        <div class="mx-auto mt-3">
                            <form id="payment-form">
                                <div id="card-element">
                                    <!-- Elements will create input elements here -->
                                </div>

                                <!-- We'll put the error messages in this element -->
                                <div id="card-errors" role="alert"></div>

                                <button id="submit" class="btn btn-primary my-2 btn-block">Pagar</button>
                            </form>
                            <form action="/paypal/contrato" method="POST" id="pago">
                                <input type="hidden" name="paypal" id="paypal" value="0">
                                @csrf
                            </form> 
                            <div id="paypal-button-container"></div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold">Información de perfil</h6>
                </div>
                <div class="card-body overflow-auto">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="POST" action="/actualizarUsuario" enctype="multipart/form-data" id="actualizar">
                        {{method_field('PUT')}}
                        @csrf
                    </form>
                    <div class="table-responsive">
                        <table class="table table-sm">
                            <tbody>
                                <tr>
                                    <th scope="row">Nombre</th>
                                    <td><input type="text" class="form-control form-control-sm" name="nombre" id="nombre" value="{{Auth::user()->nombre}}" form="actualizar"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Apellido Paterno</th>
                                    <td><input type="text" class="form-control form-control-sm" name="apellidoPaterno" id="apellidoPaterno" value="{{Auth::user()->apellidoPaterno}}" form="actualizar"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Apellido Materno</th>
                                    <td><input type="text" class="form-control form-control-sm" name="apellidoMaterno" id="apellidoMaterno" value="{{Auth::user()->apellidoMaterno}}" form="actualizar"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Nacimiento</th>
                                    <td><input type="date" class="form-control form-control-sm" name="nacimiento" id="nacimiento" value="{{Auth::user()->nacimiento}}" form="actualizar"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Correo-e</th>
                                    <td><input type="text" class="form-control form-control-sm" name="email" id="email" value="{{Auth::user()->email}}" form="actualizar"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Teléfono</th>
                                    <td><input type="text" class="form-control form-control-sm" name="telefono" id="telefono" value="{{Auth::user()->telefono}}" form="actualizar"></td>
                                </tr>
                                <tr>
                                    <th scope="row">CURP</th>
                                    <td><input type="text" class="form-control form-control-sm" name="curp" id="curp" value="{{Auth::user()->curp}}" form="actualizar"></td>
                                </tr>
                                <tr>
                                    <th scope="row">IMSS</th>
                                    <td><input type="text" class="form-control form-control-sm" name="imss" id="imss" value="{{Auth::user()->imss}}" form="actualizar"></td>
                                </tr>
                                <tr>
                                    <th scope="row">Identificación 1</th>
                                    <td>
                                        <div class="custom-file"> <input type="file" class="custom-file-input" name="identificacion" id="identificacion" form="actualizar"> <small class="custom-file-label" for="customFile">Archivo</small> </div>
                                    @if(Auth::user()->identificacion != NULL)
                                    <a href="{{Storage::url(Auth::user()->identificacion)}}">Descargar</a>
                                    @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Identificación 2</th>
                                    <td>
                                        <div class="custom-file"> <input type="file" class="custom-file-input" name="identificacion2" id="identificacion2" form="actualizar"> <small class="custom-file-label" for="customFile">Archivo</small> </div>
                                    @if(Auth::user()->identificacion2 != NULL)
                                    <a href="{{Storage::url(Auth::user()->identificacion2)}}">Descargar</a>
                                    @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Domicilio</th>
                                    <td>
                                        <div class="custom-file"> <input type="file" class="custom-file-input" name="domicilio" id="domicilio" form="actualizar"> <small class="custom-file-label" for="customFile">Archivo</small> </div>
                                    @if(Auth::user()->domicilio != NULL)
                                    <a href="{{Storage::url(Auth::user()->domicilio)}}">Descargar</a>
                                    @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">NSS</th>
                                    <td>
                                    @if(Auth::user()->nss == NULL)
                                    <span class="text-danger">Pendiente</span>
                                    @else
                                    <a href="{{Storage::url(Auth::user()->nss)}}">Descargar</a>
                                    @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <input type="submit" class="btn btn-primary mr-3" value="Actualizar" form="actualizar">
                </div>
            </div>
        </div>
    </div>

    @if(count($preguntas) > 0)
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold ">Preguntas frecuentes</h6>
                </div>
                <div class="card-body">
                @foreach($preguntas as $p)
                    <h6>{{$p->pregunta}}</h6> <br>
                    <p>{!!$p->respuesta!!}</p> <hr>
                @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif
</div>

<!-- Footer -->
<footer class="sticky-footer" style="background-color: #13322b;">
	<div class="container-fluid">
		<div class="row my-auto justify-content-center">
			<div class="col-md-4 text-center my-auto">
				<p style="color: #fff;">Mexicano seguro &copy; 2019</p>
				<p style="color: #fff;"><a href="/docs/privacidad.docx" style="color: #fff">Política de Privacidad</a></p>
				<p style="color: #fff;"><a href="/docs/condiciones.docx" style="color: #fff">Términos y condiciones</a></p>
			</div>
			<div class="col-md-4 text-center my-auto">
				<p style="color: #fff;">Para más informes llama al +52 (81)85252316</p>
				<p style="color: #fff;">Escríbenos tus dudas al correo: informes@mexicanoseguro.com</p>
			</div>
			<div class="col-md-4 text-center my-auto">
				<p class="text-white">Dirección: </p>
                <p class="text-white">{{setting('address.line1')}}</p>
                <p class="text-white">{{setting('address.line2')}}</p>
			</div>
		</div>
	</div>
</footer>
<!-- End of Footer -->
@endsection
