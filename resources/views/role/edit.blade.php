@extends('layouts.app')

@section('scripts')
<script>
$(document).ready(function(){
  $('form').on('submit', function(e){
    e.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'PUT',
        url:'/roles/' + $('#idRole').val(),
        data:{
          name : $('#name').val()
        },
        success:function(data){
          alert(data);
        }
    });
  });
});
</script>
@endsection

@section('content')
<form>
<input type="hidden" id="idRole" value="{{$role->id}}">
  <div class="form-group">
    <label for="name">Nombre</label>
    <input type="text" class="form-control" id="name" placeholder="Nombre del rol" value="{{$role->name}}">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection