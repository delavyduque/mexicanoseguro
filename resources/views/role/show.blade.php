@extends('layouts.app')

@section('content')
<h1>Rol {{$role->name}}</h1>
<button class="btn" onclick="window.history.back();">Regresar</button>
<a href="/roles/{{$role->id}}/edit" class="btn">Editar</a>
@endsection