@extends('layouts.app')

@section('scripts')
<script>
$(document).ready(function(){
  $('.deleteRole').click(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type:'DELETE',
        url:'/roles/' + $(this).attr('id'),
        success:function(data){
          alert(data);
        }
    });
  });
});
</script>
@endsection

@section('content')
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
  @foreach($roles as $r)
    <tr>
      <th scope="row">{{$r->id}}</th>
      <td><a href="/roles/{{$r->id}}">{{$r->name}}</a></td>
      <td>
        <a href="/roles/{{$r->id}}/edit/">Editar</a>
        <button class="btn btn-link deleteRole" id="{{$r->id}}">Borrar</button>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection