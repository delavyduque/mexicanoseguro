@extends('layouts.app')

@section('scripts')
<script>
    $(document).ready(function(){
        $('.eliminar_comentario').submit(function(e){
            e.preventDefault();

            if (confirm('¿Eliminar comentario?')){
                $(this).unbind('submit');
                $(this).submit();
            }
        });
    });
</script>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-10">
        <div class="card">
            <div class="card-header">Editar comentario</div>
            <div class="card-body">
                <form action="/comentario/{{ $comentario->id }}" method="post" enctype="multipart/form-data" id="guardar_comentario">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label>Imagen</label>
                        <input type="file" name="imagen" class="form-control-file">
                        
                        <img src="/storage/{{ $comentario->imagen }}" alt="" class="img-fluid">
                    </div>
                    <div class="form-group">
                        <label>Nombre de cliente</label>
                        <input type="text" name="nombre_cliente" class="form-control" required value="{{ $comentario->nombre_cliente }}">
                    </div>
                    <div class="form-group">
                        <label>Comentario</label>
                        <textarea name="contenido" class="form-control" form="guardar_comentario" required>
                            {{ $comentario->contenido }}
                        </textarea>
                    </div>
                    <button class="btn btn-success">Actualizar comentario</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
