@extends('layouts.app')

@section('scripts')
<script>
    $(document).ready(function(){
        $('.eliminar_comentario').submit(function(e){
            e.preventDefault();

            if (confirm('¿Eliminar comentario?')){
                $(this).unbind('submit');
                $(this).submit();
            }
        });
    });
</script>
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-10">
        <div class="card">
            <div class="card-header">Crear comentario</div>
            <div class="card-body">
                <form action="/comentario" method="post" enctype="multipart/form-data" id="guardar_comentario">
                    @csrf
                    <div class="form-group">
                        <label>Imagen</label>
                        <input type="file" name="imagen" class="form-control-file">
                    </div>
                    <div class="form-group">
                        <label>Nombre de cliente</label>
                        <input type="text" name="nombre_cliente" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Comentario</label>
                        <textarea name="contenido" class="form-control" form="guardar_comentario" required></textarea>
                    </div>
                    <button class="btn btn-success">Guardar comentario</button>
                </form>
            </div>
        </div>
    </div>

    @if (App\Comentario::all()->count() > 0)    
    <div class="col-10 mt-5">
        <div class="card">
            <div class="card-header">Todos los comentarios</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Nombre</th>
                                <th>Comentario</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (App\Comentario::all() as $c)
                            <tr>
                                <td>
                                    @if ($c->imagen == null)
                                    <img src="/images/imagen_cliente.png" class="img-fluid" width="70">    
                                    @else
                                    <img src="/storage/{{$c->imagen}}" class="img-fluid" width="70">
                                    @endif
                                </td>
                                <td>{{ $c->nombre_cliente }}</td>
                                <td>{{ $c->contenido }}</td>
                                <td>
                                    <form action="/comentario/{{ $c->id }}" method="post" class="eliminar_comentario">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/comentario/{{ $c->id }}/editar" class="btn btn-sm btn-info"><i class="fas fa-pencil"></i></a>
                                        <button class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>


@endsection
