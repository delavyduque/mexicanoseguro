@extends('layouts.app')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Agregar un vendedor</h1>

          <div class="row justify-content-center">

            <div class="col-lg-10">
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Datos del vendedor</h6>
                </div>
                <div class="card-body">
                    <form>
                      <div class="row">
                        <div class="form-group col-md-4">
                          <label for="exampleInputName1" data-toggle="tooltip" data-placement="top" title="Como aparece en su identificación">Nombre(s): </label>
                          <input type="text" class="form-control" id="nameInput1" placeholder="Nombre">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="exampleInputlastname1">Apellido Paterno: </label>
                          <input type="text" class="form-control" id="lastNamePInput1" placeholder="Apellido Paterno">
                        </div>
                        <div class="form-group col-md-4">
                          <label for="exampleInputlastname1">Apellido Materno: </label>
                          <input type="text" class="form-control" id="lastNameInput1" placeholder="Apellido Materno">
                        </div>
                      </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Correo electrónico: </label>
                            <input type="email" class="form-control" id="emailInput1" placeholder="correo@correo.com">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPhone1">Celular: </label>
                            <input type="tel" class="form-control" id="telInput1">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Identificación</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Foto del vendedor</label>
                            <input type="file" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                      </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End of Main Content -->

@endsection