@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script>
    $(document).ready(function(){
        $('.eliminar_sueldo').submit(function(e){
            e.preventDefault();
            if (confirm('¿Eliminar promoción? Esta acción es irreversible.')){
                $(this).unbind('submit');
                $(this).submit();
            }
        });
    });
</script>
@endsection

@section('content')

<div class="d-flex justify-content-center mb-3">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                <i class="fas fa-file-invoice"></i> Nueva promoción
            </div>
            <div class="card-body">
                <form action="/sueldo" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Costo inscripción</label>
                            <input type="number" name="inscripcion" class="form-control form-control-sm" min="0" step="0.01" required autocomplete="off">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Costo mensualidad</label>
                            <input type="number" name="mensualidad" class="form-control form-control-sm" min="0" step="0.01" required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nombre (opcional)</label>
                        <input type="text" name="nombre" class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                        <label>Código (opcional)</label>
                        <input type="text" name="codigo" class="form-control form-control-sm">
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" name="estado">
                        <label class="form-check-label">
                          Promoción principal
                        </label>
                    </div>
                    <small>Si seleccionas esta opción, esta será la promoción que aparece en la página principal</small><br>
                    <button type="submit" class="btn btn-sm btn-success mt-2">Guardar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="d-flex justify-content-center mb-3">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                <i class="fas fa-list"></i> Todas las promociones
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                            <th>Nombre</th>
                            <th>Código</th>
                            <th>Inscripción ({{ $currency }})</th>
                            <th>Mensualidad ({{ $currency }})</th>
                            <th>Principal</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @foreach (App\Sueldo::all()->sortByDesc('estado') as $s)
                            <tr>
                                <td>
                                    <a href="/sueldo/{{ $s->id }}/editar">
                                    {{ $s->nombre ?? 'Sin nombre' }}
                                    </a>
                                </td>
                                <td>{{ $s->codigo ?? 'Sin código' }}</td>
                                <td class="text-right">{{ $s->sdi }}</td>
                                <td class="text-right">{{ $s->mensualidad }}</td>
                                <td>{{ $s->estado == 1 ? 'Sí' : 'No' }}</td>
                                <td>
                                    <form action="/sueldo/{{ $s->id }}" method="POST" class="eliminar_sueldo">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
