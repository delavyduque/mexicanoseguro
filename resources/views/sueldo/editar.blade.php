@extends('layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
<script>
    $(document).ready(function(){
        $('.eliminar_sueldo').submit(function(e){
            e.preventDefault();
            if (confirm('¿Eliminar promoción? Esta acción es irreversible.')){
                $(this).unbind('submit');
                $(this).submit();
            }
        });
    });
</script>
@endsection

@section('content')

<div class="d-flex justify-content-center mb-3">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                <i class="fas fa-file-invoice"></i> Nueva promoción
            </div>
            <div class="card-body">
                <form action="/sueldo/{{ $sueldo->id }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>Costo inscripción</label>
                            <input type="number" name="inscripcion" class="form-control form-control-sm" min="0" step="0.01" required autocomplete="off" value="{{ $sueldo->sdi }}">
                        </div>
                        <div class="col-md-6 form-group">
                            <label>Costo mensualidad</label>
                            <input type="number" name="mensualidad" class="form-control form-control-sm" min="0" step="0.01" required autocomplete="off" value="{{ $sueldo->mensualidad }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nombre (opcional)</label>
                        <input type="text" name="nombre" class="form-control form-control-sm" value="{{ $sueldo->nombre }}">
                    </div>
                    <div class="form-group">
                        <label>Código (opcional)</label>
                        <input type="text" name="codigo" class="form-control form-control-sm" value="{{ $sueldo->codigo }}">
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" name="estado" {{ $sueldo->estado == 1 ? 'checked' : '' }}>
                        <label class="form-check-label">
                          Promoción principal
                        </label>
                    </div>
                    <small>Si seleccionas esta opción, esta será la promoción que aparece en la página principal</small><br>
                    <div class="mt-2">
                        <button type="submit" class="btn btn-sm btn-success">Guardar</button>
                        <a href="/sueldo" class="btn btn-sm btn-secondary">Regresar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="d-flex justify-content-center mb-3">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                <i class="fas fa-list"></i> Clientes de esta promoción
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table text-nowrap">
                        <thead>
                            <th>Nombre</th>
                            <th>Fecha corte</th>
                        </thead>
                        <tbody>
                            @foreach (App\User::where('plan_id', $sueldo->id)->get() as $u)
                            <tr>
                                <td>{{ "$u->nombre $u->apellidoPaterno" }}</td>
                                @if ($u->subscripciones->last())
                                <td>{{ Carbon\Carbon::parse($u->subscripciones->last()->hasta)->isoFormat('dd/mm/yy') ?? 'No tiene' }}</td>
                                @else
                                    <td>No tiene</td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
