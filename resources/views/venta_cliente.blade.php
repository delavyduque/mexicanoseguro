@extends('layouts.app')

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script>
    $(function(){
        var canvas = document.querySelector("canvas");
        signaturePad = new SignaturePad(canvas);

        $('#borrarFirma').click(function(e){
            e.preventDefault();
            signaturePad.clear();
        });

        $('#registro').submit(function(e){
            $('#firma').val(signaturePad.toDataURL());
        });
    });
</script>
@endsection

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h3>Regístrate</h3>
        </div>
        <div class="card-body">
            <p>Por favor, llena estos datos para registrarte en el sistema.</p>
            <p>Atentamente, {{App\User::find($vendedor_id)->nombre}}.</p>
            <form method="POST" action="/registro/venta/{{$vendedor_id}}" id="registro" enctype="multipart/form-data"  autocomplete="off">
                @csrf
                <input type="hidden" name="firma" id="firma">
                <input type="hidden" name="vendedor_id" value="{{$vendedor_id}}">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <small for="nombre" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Nombre(s)</small>
                        <input type="text" class="form-control form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre') }}" required autofocus>
                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <small for="apellidoPaterno" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Apellido Paterno</small>
                        <input type="text" class="form-control form-control{{ $errors->has('apellidoPaterno') ? ' is-invalid' : '' }}" id="apellidoPaterno" name="apellidoPaterno" value="{{ old('apellidoPaterno') }}">
                        @if ($errors->has('apellidoPaterno'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellidoPaterno') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <small for="apellidoMaterno" data-toggle="tooltip" data-placement="top" title="Como aparece en el acta de nacimiento">Apellido Materno</small>
                        <input type="text" class="form-control form-control{{ $errors->has('apellidoMaterno') ? ' is-invalid' : '' }}" id="apellidoMaterno" name="apellidoMaterno" value="{{ old('apellidoMaterno') }}">
                        @if ($errors->has('apellidoMaterno'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellidoMaterno') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <small for="email">Correo electrónico</small>
                        <input type="email" class="form-control form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                        <small for="telefono">Teléfono</small>
                        <input type="text" class="form-control form-control{{ $errors->has('telefono') ? ' is-invalid' : '' }}" id="telefono" name="telefono" value="{{ old('telefono') }}" required>
                        @if ($errors->has('telefono'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <small for="clave">Contraseña</small>
                        <input type="password" class="form-control form-control{{ $errors->has('clave') ? ' is-invalid' : '' }}" id="clave" name="clave" value="{{ old('clave') }}" required>
                        @if ($errors->has('clave'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('clave') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <small for="curp">Lugar de nacimiento</small>
                        <input type="text" class="form-control form-control{{ $errors->has('curp') ? ' is-invalid' : '' }}" id="curp" name="curp" value="{{ old('curp') }}" required>
                        @if ($errors->has('curp'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('curp') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <small for="nacimiento">Fecha de nacimiento</small>
                        <input type="date" timezone="America/Mexico_City" class="form-control form-control{{ $errors->has('nacimiento') ? ' is-invalid' : '' }}" id="nacimiento" name="nacimiento" value="{{ old('nacimiento') }}" required>
                        @if ($errors->has('nacimiento'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nacimiento') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <canvas width="400" height="100" class="text-center border mr-3"></canvas>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="form-group col-md-4">
                        <label>Código</label>
                        <input type="text" name="codigo" class="form-control">
                    </div>
                </div>
                <div class="d-flex justify-content-center">
                    <h4 class="text-center">Firma
                        <button id="borrarFirma" class="btn btn-secondary d-flex align-self-center btn-sm">Limpiar</button>
                    </h4>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary mt-2 btn-block" style="background-color: #000; border-color: #000" type="submit" form="registro">Registrarme</button>
        </div>
    </div>
</div>
@endsection