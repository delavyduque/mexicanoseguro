@extends('layouts.app')

@section('scripts')
<script src="/js/ckeditor.js"></script>
<script>
    $(function(){
        ClassicEditor.create( document.querySelector( '.editor' ), {
            toolbar: {
                items: [
                    'heading',
                    '|',
                    'bold',
                    'italic',
                    'link',
                    'bulletedList',
                    'numberedList',
                    'alignment',
                    '|',
                    'indent',
                    'outdent',
                    '|',
                    'imageUpload',
                    'blockQuote',
                    'insertTable',
                    'mediaEmbed',
                    'undo',
                    'redo'
                ]
            },
            language: 'es',
            image: {
                toolbar: [
                    'imageTextAlternative',
                    'imageStyle:full',
                    'imageStyle:side'
                ]
            },
            table: {
                contentToolbar: [
                    'tableColumn',
                    'tableRow',
                    'mergeTableCells',
                    'tableCellProperties',
                    'tableProperties'
                ]
            },
            licenseKey: '',
            
        } )
        .then( editor => {
            window.editor = editor;
        } )
        .catch( error => {
            console.error( 'Oops, something gone wrong!' );
            console.error( 'Please, report the following error in the https://github.com/ckeditor/ckeditor5 with the build id and the error stack trace:' );
            console.warn( 'Build id: byhfwwb7ddkx-k9m32smwcglx' );
            console.error( error );
        } );
    });
</script>
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="/css/ckeditor.css">
<style>
    .ck-editor__editable_inline {
        max-height: 200px;
    }
</style>
@endsection

@section('content')
<div class="container">
    <h3 class="mt-5">Editar pregunta</h3>
    <form method="POST" action="/pregunta/{{ $pregunta->id }}">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="pregunta">Pregunta</label>
            <input type="text" class="form-control form-control{{ $errors->has('pregunta') ? ' is-invalid' : '' }}" name="pregunta" value="{{ $pregunta->pregunta }}" autofocus>
            @if ($errors->has('pregunta'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('pregunta') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="respuesta">Respuesta</label>
            <textarea class="editor" name="respuesta" cols="30" rows="10">{!! $pregunta->respuesta !!}</textarea>
            @if ($errors->has('respuesta'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('respuesta') }}</strong>
                </span>
            @endif
        </div>
        <div>
            <button class="btn btn-primary" type="submit">Guardar</button>
            <a href="#" onclick="window.history.back()" class="btn btn-secondary">Regresar</a>
        </div>
    </form>
</div>
@endsection