@component('mail::message')
# Estimado {{$usuario->nombre}}:

@if($clave !== NULL)
Te damos una cordial bienvenida a {{ env('APP_NAME') }}.

Se ha generado una contraseña segura para ti:

## <center>{{$clave}}</center>

@endif

Puedes acceder a la plataforma con tu correo y contraseña dando clic en el siguiente botón:

@component('mail::button', ['url' => env('APP_URL') . '/login'])
Entrar
@endcomponent

@if($ventas !== NULL)
Tu suscripción se ha registrado para los siguientes periodos

@component('mail::table')
| Periodo |
|:-------:|
@foreach($ventas as $v)
| {{Carbon\Carbon::parse($v->desde)->isoFormat('DD MMM Y')}} al {{Carbon\Carbon::parse($v->hasta)->isoFormat('DD MMM Y')}}|
@endforeach()
@endcomponent

@endif

Atentamente, <br> 
{{ env('APP_NAME') }}
@endcomponent