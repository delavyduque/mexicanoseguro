@component('mail::message')
# Reporte de actividad 

Reporte diario del {{\Carbon\Carbon::today()->addDays(-1)->isoFormat('D [de] MMMM [de] YYYY')}}.

@if($ventas->count() > 0)
@component('mail::table')
| # | Nombre | Promoción | CURP | IMSS | NSS | Movimiento |
|:-:|:-------|:---------:|:-----|:-----|:----|:-----------|
@foreach($ventas as $v)
| {{$v->cliente_id}} | {{$v->cliente->nombre}} {{$v->cliente->apellidoPaterno}} {{$v->cliente->apellidoMaterno}} | {{App\Sueldo::find($v->cliente->plan_id)->nombre ?? App\Sueldo::where('estado', 1)->first()->nombre}} | {{$v->cliente->curp ?? 'Pendiente'}} | {{$v->cliente->imss ?? 'Pendiente'}} | {{ ' ' }} | <span style="color:{{$v->tipo == 'Baja' ? 'red' : 'blue'}}">{{$v->tipo}}</span> |
@endforeach
@endcomponent
@else
Ayer no se dieron altas ni bajas.
@endif

{{ env('APP_NAME') }}
@endcomponent
