<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Contrato Mexicano Seguro</title>
 <style>
   .container{
      padding: 5%;
   }
   p{
    text-align: justify;
   }
</style>
</head>
<body>
    <div class="container">
    <h2>Contrato Mexicano Seguro</h2>
    <p>Contrato de Confidencialidad, que celebran, por una parte Mexicano Seguro representada en este acto por Monserrat Mazatini, y a quien en lo sucesivo y por razones de brevedad se le denominará “LA EMPRESA” y, por la otra, por su propio derecho, <b>{{$usuario->nombre}} {{$usuario->apellidoPaterno}} {{$usuario->apellidoMaterno}}</b>, a quien en lo sucesivo se le denominará “EL COLABORADOR”, y LAS PARTES, para referirse tanto a LA EMPRESA como al Empleado, al tenor de las siguientes:</p>

    <p>DECLARACIONES</p>
    <p>I. DECLARA “LA EMPRESA”: </p>

    <p>a. Que ha desarrollado cierta información técnica, tecnológica, comercial, de fabricación y de negocios y cuenta con la organización necesaria para cumplir con sus objetivos técnicos, tecnológicos, industriales, administrativos, comerciales y de negocios, y que ocasionalmente obtiene, por la vía legal, cierta “Información” de terceros.</p>

    <p>II. DECLARA “EL COLABORADOR”: </p>

    <p>a. Que su nombre es {{$usuario->nombre}} {{$usuario->apellidoPaterno}} {{$usuario->apellidoMaterno}} ser ciudadano mexicano, mayor de edad y con capacidad suficiente para ejercer sus derechos y obligaciones;</p>

    <p>b. Que, como empleado de “LA EMPRESA”, está consciente de sus obligaciones de confidencialidad de los secretos técnicos, comerciales y de fabricación de los cuales tiene conocimiento por razón de su empleo, establecidos en el Artículo 134 Fracción XIII de la Ley Federal del Trabajo.</p>

    <p>c. Que está consciente que la violación de tales obligaciones de confidencialidad, configuran los delitos establecidos por el Articulo 223 Fracciones III y IV de la Ley de la Propiedad Industrial y por los Artículos 210, 211 y 211 bis del Código Penal para el Distrito Federal, y los correlativos de todos los Estados de la República Mexicana; así como las sanción establecida en el Artículo 224 de la Ley de la Propiedad Industrial, en los mismos Artículos 210, 211 y 212 bis del Código Penal para el Distrito Federal y en el Artículo 47 de la Ley Federal de Trabajo.</p>

    <p>III. Declaran LAS PARTES:</p>

    <p>a. Ambas partes reconocen mutuamente la personalidad que ostentan y con fundamento en las anteriores DECLARACIONES, expresan que es su libre voluntad celebrar en presente Contrato conforme a las siguientes:</p>

    <p>CLÁUSULAS</p>

    <p>PRIMERA: EL EMPLEADO reconoce que el uso y manejo de la información financiera, estratégica, ventas, planes de desarrollo de productos, políticas de precios, mercadotecnia, sistemas de distribución, proyectos, etc., a que tiene acceso con motivo del desempeño de su actividad, constituye para la empresa donde presta sus servicios, una ventaja competitiva frente a terceros, clientes, proveedores, o cualquier persona física o moral, etc., y por ende, es de carácter confidencial, y por ello, desde ahora, se obliga y compromete a no divulgarla o transmitirla, ni en forma verbal o escrita, ni por ningún otro medio a terceros, (persona física o moral), o unidades económicas, ya constituidas operando o no, o que estén en vías de operación o constitución, ni a utilizarla para fines distintos o ajenos al desarrollo de mi trabajo y actividades conexas que me encomienda la empresa, asumiendo si responsabilidad por los daños y perjuicios que resulten, independientemente de la responsabilidad penal en que incurra por el quebrantamiento del presente compromiso, haciéndose sabedor del contenido de los artículos 82,85,86,223, Fracc. IV, 224 y 226, de la Ley de Propiedad Industrial.</p>

    <p>SEGUNDA. La Información Confidencial es y será propiedad exclusiva de “LA EMPRESA” y, por tanto, EL EMPLEADO se obliga a utilizar la Información que reciba, exclusivamente para ejecutar su trabajo como empleado de “LA EMPRESA”  y su uso para cualquier otro fin queda plena y totalmente excluido y prohibido, con las consecuencias correspondientes que marca la legislación aplicable incluyendo la establecida en las declaraciones.</p>

    <p>TERCERA. En caso de incumplimiento de la confidencialidad y prohibición anteriormente establecidas, se le aplicara a “EL COLABORADOR” una PENA CONVENCIONAL de $250, 000.00 pesos (Doscientos Cincuenta Mil pesos m.n. 00/100) por los daños y perjuicios causados, independientemente de las sanciones corporales y pecuniarias a que se hará acreedor de conformidad con lo establecido por la Ley de Fomento y Protección de la Propiedad Industrial y su reglamento, así como los demás ordenamientos legales aplicables al caso.</p>

    <p>CUARTA. - La presente responsabilidad de “EL COLABORADOR” adquirida con este instrumento las partes acuerdan que será por la duración de la Relación Laboral, y se extiende hasta un termino no menor de 05 año después de haber terminado el vínculo laboral antes mencionado.</p>

    <p>QUINTA. - Para los efectos de interpretación y cumplimiento del presente Contrato, las Partes se someten expresamente a la Jurisdicción de los Tribunales competentes de la Monterrey, Nuevo Leon, renunciando a cualquier otro que les pudiera corresponder en razón de su domicilio presente o futuro.</p>
    <div style="text-align:center">
      <img src="{!!$usuario->firma!!}" class="border-bottom border-dark">
    </div>
    <div style="text-align:center">
      Firma del cliente
    </div>
  </div>
</body>
</html>