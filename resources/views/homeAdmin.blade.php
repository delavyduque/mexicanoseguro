@extends('layouts.app')
@section('scripts')
<script>
$(function() {
    $("#inputGroupSelect01").val(1);
});
</script>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Pagos realizados</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                <th>Cliente</th>
                                <th>Pago (dólares)</th>
                                <th>Concepto</th>
                                <th>Fecha de pago</th>
                                <th>Suscripción</th>
                                <th>Vendedor</th>
                                <th>Acciones</th>
                                </tr>
                             </thead>
                             <tbody>
                                <tr>
                                <td>Jesús Rangel Reverte</td>
                                <td>100</td>
                                <td>Suscripción</td>
                                <td>04/05/2019</td>
                                <td>Mensual</td>
                                <td>Agustín Viñas</td>
                                <td><a href="#" data-toggle="modal" data-target="#paymentModal">Eliminar</a></td></td>
                                </tr>
                                <tr>
                                <td>Jesús Rangel Reverte</td>
                                <td>100</td>
                                <td>Suscripción</td>
                                <td>04/05/2019</td>
                                <td>Mensual</td>
                                <td>Agustín Viñas</td>
                                <td><a href="#" data-toggle="modal" data-target="#paymentModal">Eliminar</a></td></td>
                                </tr>
                                <tr>
                                <td>Jesús Rangel Reverte</td>
                                <td>100</td>
                                <td>Suscripción</td>
                                <td>04/05/2019</td>
                                <td>Mensual</td>
                                <td>Agustín Viñas</td>
                                <td><a href="#" data-toggle="modal" data-target="#paymentModal">Eliminar</a></td></td>
                                </tr>
                                <tr>
                                <td>Jesús Rangel Reverte</td>
                                <td>100</td>
                                <td>Suscripción</td>
                                <td>04/05/2019</td>
                                <td>Mensual</td>
                                <td>Agustín Viñas</td>
                                <td><a href="#" data-toggle="modal" data-target="#paymentModal">Eliminar</a></td></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Crear reporte</button>    
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Cambiar la cantidad de pago</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Estas son las instrucciones para cambiar la cantidad de pago. 
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal-->
                         <!-- Modal -->
                         <div class="modal fade" id="suscriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Cambiar la suscripción</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="inputGroupSelect01">Cambio de suscripción: Pagar cada</label>
                                        <div>
                                            <select class="custom-select" id="inputGroupSelect01" name="inputGroupSelect01">
                                            <option selected>Escoja una cantidad</option>
                                            <option value="1">1 </option>
                                            <option value="2">3</option>
                                            <option value="3">6</option>
                                            <option value="3">12</option>
                                            </select>
                                        </div>
                                    <label for="inputGroupSelect01">meses</label>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal-->

                    <!-- Modal -->
                    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="paymentModalLabel">Eliminar usuario</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    ¿Estás seguro que deseas eliminar al usuario?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                    <button type="button" class="btn btn-primary">Sí</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Pagos pendientes</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                <th>Cliente</th>
                                <th>Pago (dólares)</th>
                                <th>Concepto</th>
                                <th>Fecha de pago</th>
                                <th>Suscripción</th>
                                <th>Vendedor</th>
                                <th>Acciones</th>
                                </tr>
                             </thead>
                             <tbody>
                                <tr>
                                <td>Antonio Acevedo</td>
                                <td>100</td>
                                <td>Suscripción</td>
                                <td>04/05/2019</td>
                                <td>Mensual</td>
                                <td>Agustín Viñas</td>
                                <td><a href="#" data-toggle="modal" data-target="#paymentModal">Eliminar</a></td></td>
                                </tr>
                                <tr>
                                <td>Antonio Acevedo</td>
                                <td>100</td>
                                <td>Mensualidad</td>
                                <td>04/06/2019</td>
                                <td>Mensual</td>
                                <td>Agustín Viñas</td>
                                <td><a href="#" data-toggle="modal" data-target="#paymentModal">Eliminar</a></td></td>
                                </tr>
                                <tr>
                                <td>Antonio Acevedo</td>
                                <td>100</td>
                                <td>Mensualidad</td>
                                <td>04/07/2019</td>
                                <td>Mensual</td>
                                <td>Agustín Viñas</td>
                                <td><a href="#" data-toggle="modal" data-target="#paymentModal">Eliminar</a></td></td>
                                </tr>
                                <tr>
                                <td>Antonio Acevedo</td>
                                <td>100</td>
                                <td>Mensualidad</td>
                                <td>04/08/2019</td>
                                <td>Mensual</td>
                                <td>Agustín Viñas</td>
                                <td><a href="#" data-toggle="modal" data-target="#paymentModal">Eliminar</a></td></td>
                                </tr>
                            </tbody>
                        </table>
                    <div class="row justify-content-center">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Crear reporte</button>    
                    </div>
                   
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Cambiar la cantidad de pago</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Estas son las instrucciones para cambiar la cantidad de pago. 
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal-->
                         <!-- Modal -->
                         <div class="modal fade" id="suscriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Cambiar la suscripción</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="inputGroupSelect01">Cambio de suscripción: Pagar cada</label>
                                        <div>
                                            <select class="custom-select" id="inputGroupSelect01" name="inputGroupSelect01">
                                            <option selected>Escoja una cantidad</option>
                                            <option value="1">1 </option>
                                            <option value="2">3</option>
                                            <option value="3">6</option>
                                            <option value="3">12</option>
                                            </select>
                                        </div>
                                    <label for="inputGroupSelect01">meses</label>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-primary">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal-->

                    <!-- Modal -->
                    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="paymentModalLabel">Eliminar usuario</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    ¿Estás seguro que deseas eliminar al usuario?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                    <button type="button" class="btn btn-primary">Sí</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
