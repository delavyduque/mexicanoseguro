<?php

return [

    // All the sections for the settings page
    'sections' => [
        'app' => [
            'title' => 'Plataforma',
            'descriptions' => 'Configuraciones generales.', // (optional)
            'icon' => 'fa fa-cog', // (optional)

            'inputs' => [
                [
                    'name' => 'app_name', // unique key for setting
                    'type' => 'text', // type of input can be text, number, textarea, select, boolean, checkbox etc.
                    'label' => 'Nombre del sitio', // label for input
                    // optional properties
                    'placeholder' => 'Nombre del sitio', // placeholder for input
                    'class' => 'form-control', // override global input_class
                    'style' => '', // any inline styles
                    'rules' => 'required|min:2|max:20', // validation rules for this input
                    'value' => 'Mexicano Seguro', // any default value
                    'hint' => 'Puedes escribir el nombre de tu sitio aquí' // help block text for input
                ],
                [
                    'name' => 'front1',
                    'type' => 'image',
                    'label' => 'Banner 1',
                    'hint' => 'Imagen de 710 x 340',
                    'rules' => 'image|max:2048',
                    'disk' => 'public', // which disk you want to upload, default to 'public'
                    'path' => 'app', // path on the disk, default to '/',
                    'preview_class' => 'thumbnail', // class for preview of uploaded image
                    'preview_style' => 'height:40px' // style for preview
                ],
                [
                    'name' => 'front2',
                    'type' => 'image',
                    'label' => 'Banner 2',
                    'hint' => 'Imagen de 710 x 340',
                    'rules' => 'image|max:2048',
                    'disk' => 'public', // which disk you want to upload, default to 'public'
                    'path' => 'app', // path on the disk, default to '/',
                    'preview_class' => 'thumbnail', // class for preview of uploaded image
                    'preview_style' => 'height:40px' // style for preview
                ],
                [
                    'name' => 'front3',
                    'type' => 'image',
                    'label' => 'Banner 3',
                    'hint' => 'Imagen de 710 x 340',
                    'rules' => 'image|max:2048',
                    'disk' => 'public', // which disk you want to upload, default to 'public'
                    'path' => 'app', // path on the disk, default to '/',
                    'preview_class' => 'thumbnail', // class for preview of uploaded image
                    'preview_style' => 'height:40px' // style for preview
                ],
                [
                    'name' => 'desc',
                    'type' => 'image',
                    'label' => 'Qué es',
                    'hint' => 'Imagen de 1050 x 960',
                    'rules' => 'image|max:2048',
                    'disk' => 'public', // which disk you want to upload, default to 'public'
                    'path' => 'app', // path on the disk, default to '/',
                    'preview_class' => 'thumbnail', // class for preview of uploaded image
                    'preview_style' => 'height:40px' // style for preview
                ],
                [
                    'name' => 'description',
                    'type' => 'textarea',
                    'label' => '¿Qué es?',
                    'rows' => 4,
                    'cols' => 10,
                    'placeholder' => 'Texto del apartado ¿Qué es?'
                ],
                [
                    'name' => 'imss_img',
                    'type' => 'image',
                    'label' => 'IMSS arriba',
                    'hint' => 'Imagen de 1050 x 960',
                    'rules' => 'image|max:2048',
                    'disk' => 'public', // which disk you want to upload, default to 'public'
                    'path' => 'app', // path on the disk, default to '/',
                    'preview_class' => 'thumbnail', // class for preview of uploaded image
                    'preview_style' => 'height:40px' // style for preview
                ],
                [
                    'name' => 'imss_img2',
                    'type' => 'image',
                    'label' => 'IMSS abajo',
                    'hint' => 'Imagen de 1050 x 960',
                    'rules' => 'image|max:2048',
                    'disk' => 'public', // which disk you want to upload, default to 'public'
                    'path' => 'app', // path on the disk, default to '/',
                    'preview_class' => 'thumbnail', // class for preview of uploaded image
                    'preview_style' => 'height:40px' // style for preview
                ],
                [
                    'name' => 'imss',
                    'type' => 'textarea',
                    'label' => 'IMSS',
                    'rows' => 4,
                    'cols' => 10,
                    'placeholder' => 'Texto del apartado IMSS'
                ],
                [
                    'name' => 'infonavit_img',
                    'type' => 'image',
                    'label' => 'Infonavit',
                    'hint' => 'Imagen de 1050 x 960',
                    'rules' => 'image|max:2048',
                    'disk' => 'public', // which disk you want to upload, default to 'public'
                    'path' => 'app', // path on the disk, default to '/',
                    'preview_class' => 'thumbnail', // class for preview of uploaded image
                    'preview_style' => 'height:40px' // style for preview
                ],
                [
                    'name' => 'infonavit',
                    'type' => 'textarea',
                    'label' => 'INFONAVIT',
                    'rows' => 4,
                    'cols' => 10,
                    'placeholder' => 'Texto del apartado INFONAVIT'
                ],
                [
                    'name' => 'pension_img',
                    'type' => 'image',
                    'label' => 'Pensión',
                    'hint' => 'Imagen de 1050 x 960',
                    'rules' => 'image|max:2048',
                    'disk' => 'public', // which disk you want to upload, default to 'public'
                    'path' => 'app', // path on the disk, default to '/',
                    'preview_class' => 'thumbnail', // class for preview of uploaded image
                    'preview_style' => 'height:40px' // style for preview
                ],
                [
                    'type' => 'textarea',
                    'name' => 'oferta',
                    'label' => 'Oferta',
                    'rows' => 4,
                    'cols' => 10,
                    'placeholder' => 'Texto del apartado Oferta'
                ],
            ]
        ],
        'phone' => [
            'title' => 'Teléfonos',
            'descriptions' => 'Teléfonos empresariales.', // (optional)
            'icon' => 'fas fa-phone', 

            'inputs' => [
                [
                    'name' => 'tel_ventas', // unique key for setting
                    'type' => 'text', // type of input can be text, number, textarea, select, boolean, checkbox etc.
                    'label' => 'Ventas', // label for input
                    // optional properties
                    'class' => 'form-control', // override global input_class
                    'style' => '', // any inline styles
                    'rules' => 'min:2|max:30', // validation rules for this input
                    'hint' => 'Este teléfono se verá en la parte superior' // help block text for input
                ],
                [
                    'name' => 'tel_informes', // unique key for setting
                    'type' => 'text', // type of input can be text, number, textarea, select, boolean, checkbox etc.
                    'label' => 'Informes', // label for input
                    // optional properties
                    'class' => 'form-control', // override global input_class
                    'style' => '', // any inline styles
                    'rules' => 'min:2|max:30', // validation rules for this input
                    'hint' => 'Este teléfono aparece en la parte inferior' // help block text for input
                ],
            ]
        ],
        'address' => [
            'title' => 'Dirección fiscal',
            'descriptions' => 'Dirección fiscal de la empresa.', // (optional)
            'icon' => 'fas fa-map-marker', 

            'inputs' => [
                [
                    'name' => 'line1', // unique key for setting
                    'type' => 'text', // type of input can be text, number, textarea, select, boolean, checkbox etc.
                    'label' => 'Línea 1', // label for input
                    // optional properties
                    'placeholder' => 'Calle #123, Colonia, C. P.', // placeholder for input
                    'class' => 'form-control', // override global input_class
                    'style' => '', // any inline styles
                    'rules' => 'min:2|max:30', // validation rules for this input
                    'hint' => 'Especifica aquí la calle, colonia y código postal' // help block text for input
                ],
                [
                    'name' => 'line2', // unique key for setting
                    'type' => 'text', // type of input can be text, number, textarea, select, boolean, checkbox etc.
                    'label' => 'Línea 2', // label for input
                    // optional properties
                    'placeholder' => 'Estado, País', // placeholder for input
                    'class' => 'form-control', // override global input_class
                    'style' => '', // any inline styles
                    'rules' => 'min:2|max:30', // validation rules for this input
                    'hint' => 'Especifica aquí el estado y país' // help block text for input
                ],
            ]
        ],
        'email' => [
            'title' => 'Correo',
            'icon' => 'fa fa-envelope',

            'inputs' => [
                [
                    'type' => 'textarea',
                    'name' => 'admin_mails',
                    'label' => 'Correos de administradores',
                    'rows' => 4,
                    'cols' => 10,
                    'placeholder' => 'ejemplo@correo.com',
                    'hint' => 'Escribe cada correo en una línea nueva' // help block text for input
                ],
                [
                    'type' => 'textarea',
                    'name' => 'info_mails',
                    'label' => 'Correos de información',
                    'rows' => 4,
                    'cols' => 10,
                    'placeholder' => 'ejemplo@correo.com',
                    'hint' => 'Escribe cada correo en una línea nueva' // help block text for input
                ],
            ]
        ],
    ],

    // Setting page url, will be used for get and post request
    'url' => 'settings',

    // Any middleware you want to run on above route
    'middleware' => ['auth', 'rol:1'],

    // View settings
    'setting_page_view' => 'app_settings::settings_page',
    'flash_partial' => 'app_settings::_flash',

    // Setting section class setting
    'section_class' => 'card mb-3',
    'section_heading_class' => 'card-header',
    'section_body_class' => 'card-body',

    // Input wrapper and group class setting
    'input_wrapper_class' => 'form-group',
    'input_class' => 'form-control',
    'input_error_class' => 'has-error',
    'input_invalid_class' => 'is-invalid',
    'input_hint_class' => 'form-text text-muted',
    'input_error_feedback_class' => 'text-danger',

    // Submit button
    'submit_btn_text' => 'Guardar cambios',
    'submit_success_message' => 'Se han guardado los cambios.',

    // Remove any setting which declaration removed later from sections
    'remove_abandoned_settings' => false,

    // Controller to show and handle save setting
    'controller' => '\QCod\AppSettings\Controllers\AppSettingController',

    // settings group
    'setting_group' => function() {
        // return 'user_'.auth()->id();
        return 'default';
    }
];
