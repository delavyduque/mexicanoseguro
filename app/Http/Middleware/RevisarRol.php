<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

class RevisarRol
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role_id)
    {
        if (Auth::user()->role_id > $role_id) {
            return redirect('/home');
        }

        return $next($request);
    }
}
