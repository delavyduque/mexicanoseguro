<?php

namespace App\Http\Controllers;

use App\Comentario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ComentarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('rol:2');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('comentario.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $c = new Comentario;
        if (isset($request->imagen)){
            $c->imagen = $request->file('imagen')->store('comentarios', 'public');
        }
        $c->nombre_cliente = $request->nombre_cliente;
        $c->contenido = $request->contenido;
        $c->save();

        return back()->with('mensaje', 'Comentario guardado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comentario  $comentario
     * @return \Illuminate\Http\Response
     */
    public function show(Comentario $comentario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comentario  $comentario
     * @return \Illuminate\Http\Response
     */
    public function edit(Comentario $comentario)
    {
        return view('comentario.edit', [
            "comentario" => $comentario
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comentario  $comentario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comentario $comentario)
    {
        if (isset($request->imagen)){
            try {
                Storage::disk('public')->delete($comentario->imagen);
            } catch (\Throwable $th) {
                Log::warning("Imagen de comentario $comentario->id no encontrada.");
            }
            $comentario->imagen = $request->file('imagen')->store('comentarios', 'public');
        }
        $comentario->nombre_cliente = $request->nombre_cliente;
        $comentario->contenido = $request->contenido;
        $comentario->save();

        return redirect('/comentario')->with('mensaje', 'Comentario actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comentario  $comentario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comentario $comentario)
    {
        if ($comentario->imagen != null){
            Storage::disk('public')->delete($comentario->imagen);
        }
        $comentario->delete();

        return redirect('/comentario')->with('mensaje', 'Comentario eliminado');
    }
}
