<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Venta;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Notifications\NuevoPago;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PaypalController extends Controller
{
    protected $apiContext;

    function contrato(Request $request){
        $v = Auth::user()->subscripciones->last();
        if($v != null && $v->cantidad == -1){
            $venta = $v;
        } else {
            $venta = new Venta;
        }
        $venta->cliente_id = Auth::user()->id;
        $last = Auth::user()->subscripciones->last();
        if($last != null && Carbon::parse($last->hasta) > Carbon::now()) {
            $venta->desde = Carbon::parse($last->hasta)->addDay();
            $venta->hasta = Carbon::parse($last->hasta)->addDay()->addMonth();
        } else{
            $venta->desde = Carbon::now();
            $venta->hasta = Carbon::now()->addMonth();
        }
        $venta->cantidad = 0;
        $venta->save();

        return redirect('/home')->with('mensaje', 'Pago exitoso');
    }

    function contratar(Request $request){
        if (isset($request->success) && $request->success == 'true') {
            $token = $request->token;
            $agreement = new \PayPal\Api\Agreement();
            $agreement->execute($token, $this->apiContext);

            if(Auth::user()->role_id == 4){
                $info = new \stdClass();
                $info->ventas = collect();

                $venta = new Venta;
                if(Venta::where('cliente_id', Auth::id())->where('cantidad', -1)->count() > 0){
                    $venta = Venta::where('cliente_id', Auth::id())->where('cantidad', -1)->first();
                }
                $venta->cliente_id = Auth::user()->id;
                $venta->desde = Carbon::now();
                $venta->hasta = Carbon::now()->addMonth();
                $venta->cantidad = 0;
                $venta->billing_id = $agreement->getId();
                $venta->save();

                $info->ventas->push($venta);
                $info->usuario = Auth::user();
                $info->clave = NULL;

                Auth::user()->notify(new NuevoPago($info));

                return redirect('/home')->with('mensaje', 'Compra realizada');
            } else {
                $venta = Venta::all()->where('vendedor_id', Auth::id())->last();
                $venta->billing_id = $agreement->getId();
                $venta->save();

                $usuario = User::find($venta->cliente_id);

                $info = new \stdClass();
                $info->clave = $request->clave;
                $info->usuario = $usuario;
                $info->ventas = collect([$venta]);

                $usuario->notify(new NuevoPago($info));
                return redirect("/venta")->with('mensaje', 'Venta y usuario registrados.');
            }
        } else{
            if (Auth::user()->role_id == 4) {
                return redirect('/home')->with('mensaje', 'Operación cancelada');
            } else {
                $venta = Venta::all()->where('vendedor_id', Auth::id())->last();
                $venta->delete();
                return redirect("/venta")->with('mensaje', 'Operación cancelada.');
            }
        }
    }

    function contratarStripe(Request $request){
        if (isset($request->session_id)){
            if($request->session_id == session('stripe')->id){
                if (Auth::user()->role_id != 4){
                    $info = new \stdClass();
                    $info->ventas = collect();

                    $venta = Auth::user()->ventas->last();
                    $venta->desde = Carbon::now();
                    $venta->hasta = Carbon::now()->addMonth();
                    $venta->save();

                    $info->ventas->push($venta);

                    
                    $clave = Str::random(8);
                    $venta->cliente->password = Hash::make($clave);
                    $venta->cliente->save();

                    $info->usuario = $venta->cliente;
                    $info->clave = $clave;

                    $venta->cliente->notify(new NuevoPago($info));

                    return redirect('/venta')->with('message', 'Subscripción registrada');
                } else {
                    $info = new \stdClass();
                    $info->ventas = collect();
    
                    $venta = new Venta;
                    if(Venta::where('cliente_id', Auth::id())->where('cantidad', -1)->count() > 0){
                        $venta = Venta::where('cliente_id', Auth::id())->where('cantidad', -1)->first();
                    }
                    $venta->cliente_id = Auth::user()->id;
    
                    $last = Auth::user()->subscripciones->last();
                    if($last != null && Carbon::parse($last->hasta) > Carbon::now()) {
                        $venta->desde = Carbon::parse($last->hasta)->addDay();
                        $venta->hasta = Carbon::parse($last->hasta)->addDay()->addMonth();
                    } else{
                        $venta->desde = Carbon::now();
                        $venta->hasta = Carbon::now()->addMonth();
                    }
    
                    $venta->cantidad = session('stripe')->display_items[0]->amount / 100;
                    $venta->save();
    
                    $info->ventas->push($venta);
                    $info->usuario = Auth::user();
                    $info->clave = NULL;
    
                    Auth::user()->notify(new NuevoPago($info));
    
                    return redirect('/home')->with('mensaje', 'Compra realizada');
                }
            } else{
                return redirect('/home')->with('message', 'Vuelve a intentar');
            }
        } else {
            return redirect('/home')->with('message', 'Vuelve a intentar');
        }
    }
}
