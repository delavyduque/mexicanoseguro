<?php

namespace App\Http\Controllers;

use App\Pregunta;
use Illuminate\Http\Request;

class PreguntaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('rol:1');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preguntas = Pregunta::all();
        return view('pregunta.index', ['preguntas' => $preguntas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pregunta.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'pregunta' => ['required', 'string'],
            'respuesta' => ['required', 'string']
        ]);

        $pregunta = new Pregunta;
        $pregunta->pregunta = $request->pregunta;
        $pregunta->respuesta = $request->respuesta;
        $pregunta->save();

        return redirect("/pregunta")->with('mensaje', 'Pregunta ' . $pregunta->id . ' creada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pregunta  $pregunta
     * @return \Illuminate\Http\Response
     */
    public function show(Pregunta $pregunta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pregunta  $pregunta
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pregunta = Pregunta::find($id);

        if ($pregunta){
            return view('pregunta.edit', [
                'pregunta' => $pregunta
            ]);
        } else {
            return redirect('/pregunta')->with('mensaje', 'Pregunta no encontrada');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pregunta  $pregunta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pregunta = Pregunta::find($id);
        
        if ($pregunta){
            $pregunta->pregunta = $request->pregunta;
            $pregunta->respuesta = $request->respuesta;
            $pregunta->save();

            return redirect('/pregunta')->with('mensaje', 'Pregunta actualizadas');
        } else {
            return redirect('/pregunta')->with('mensaje', 'Pregunta no encontrada');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pregunta  $pregunta
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pregunta = Pregunta::find($id);

        if ($pregunta){
            $pregunta->delete();
    
            return redirect('/pregunta')->with('mensaje', 'Pregunta eliminada');
        } else {
            return redirect('/pregunta')->with('mensaje', 'Pregunta no encontrada');
        }
    }
}
