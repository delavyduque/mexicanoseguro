<?php

namespace App\Http\Controllers;

use App\Notifications\NuevoPago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Venta;
use App\User;
use App\Sueldo;
use Illuminate\Support\Facades\Auth;
use Storage;
use Carbon\Carbon;

class VentaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('rol:3')->except('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rol = Auth::user()->role_id;
        $currency = Str::contains(env('APP_URL'), 'ahoramexicano') ? 'MXN' : 'USD';

        if ($rol <= 2) {
            $ventas = Venta::all();
            return view('venta.index', ['ventas' => $ventas, 'currency' => $currency]);
        } else if ($rol == 3) {
            $ventas = Venta::where('vendedor_id', Auth::user()->id)->get();
            return view('venta.index', ['ventas' => $ventas, 'currency' => $currency]);
        } else if ($rol == 5){
            $u = User::where('id', Auth::id())->orWhere('promotor_id', Auth::id())->get();
            $ventas = $u->pluck('ventas')->flatten();
            return view('venta.index_promotor', ['ventas' => $ventas]);
        } else{
            return redirect('/home')->with('mensaje', 'Acceso denegado');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // https://github.com/paypal/PayPal-PHP-SDK/issues/298#issuecomment-97991805
        $currency = Str::contains(env('APP_URL'), 'ahoramexicano') ? 'MXN' : 'USD';
        
        $plan = Sueldo::where('estado', 1)->first() ?? Sueldo::first();
        
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

        $intent = \Stripe\PaymentIntent::create([
            'amount' => 100 * ($plan->mensualidad + $plan->sdi),
            'currency' => Str::contains(env('APP_URL'), 'ahoramexicano') ? 'mxn' : 'usd',
            // Verify your integration in this guide by including this parameter
            'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);
        
        return view('venta.create', [
            'plan' => $plan,
            'currency' => $currency,
            'client_secret' => $intent->client_secret
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        $clave = Str::random(8);
        $usuario = new User;
        $usuario->nombre = $request->nombre;
        $usuario->apellidoPaterno = $request->apellidoPaterno;
        $usuario->apellidoMaterno = $request->apellidoMaterno;
        $usuario->email = $request->email;
        $usuario->telefono = $request->telefono;
        $usuario->nacimiento = $request->nacimiento;
        $usuario->curp = $request->curp;
        $usuario->imss = $request->imss;
        $usuario->firma = $request->firma;

        if ($request->identificacion){
            $path = Storage::putFile('ine', $request->identificacion);
        } else{
            $path = "";
        }
        $usuario->identificacion = $path;

        if ($request->identificacion2){
            $path = Storage::putFile('ine', $request->identificacion2);
        } else{
            $path = "";
        }
        $usuario->identificacion2 = $path;

        if ($request->domicilio){
            $path = Storage::putFile('ine', $request->domicilio);
        } else{
            $path = "";
        }
        $usuario->domicilio = $path;

        $usuario->password = Hash::make($clave);
        $usuario->role_id = 4;
        $usuario->identificacion = $request->identificacion;

        $usuario->save();

        $venta = new Venta;
        $venta->cliente_id = $usuario->id;
        $venta->vendedor_id = Auth::user()->id;
        $venta->cantidad = 0;
        $venta->desde = Carbon::now();
        $venta->hasta = Carbon::now()->addMonth();
        $venta->save();

        $info = new \stdClass();
        $info->usuario = $usuario;
        $info->clave = $clave;
        $info->ventas = collect();
        $info->ventas->push($venta);

        $usuario->notify(new NuevoPago($info));

        return redirect('/venta')->with('message', 'Subscripción registrada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        $usuario = User::find($id);
        return view('usuario.edit', ['usuario' => $usuario]);
        */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $venta = Venta::find($id);
        $venta->delete();

        return redirect("/venta")->with('mensaje', 'Venta eliminada.');

    }
}
