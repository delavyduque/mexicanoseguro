<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Illuminate\Notifications\Notifiable;
use App\Notifications\NuevoPago;
use App\Sueldo;
use App\User;
use Illuminate\Support\Facades\Auth;
use Storage;
use Illuminate\Support\Str;

class UsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('rol:2')->except(['show', 'store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::where('role_id', '!=', 4)->get()->where('id', '>', 1);
        return view('usuario.index', ['usuarios' => $usuarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuario.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->role_id == 5){
            $request->validate([
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            ]);
            $clave = Str::random(8);
            $usuario = new User;
            $usuario->nombre = $request->nombre;
            $usuario->apellidoPaterno = $request->apellidoPaterno;
            $usuario->apellidoMaterno = $request->apellidoMaterno;
            $usuario->email = $request->email;
            $usuario->password = Hash::make($clave);
            $usuario->role_id = 3;
            $usuario->promotor_id = Auth::id();
            $usuario->save();

            $info = new \stdClass();
            $info->clave = $clave;
            $info->usuario = $usuario;
            $info->ventas = null;

            $usuario->notify(new NuevoPago($info));

            return redirect('/venta')->with('mensaje', 'Vendedor inscrito.');
        } else if(Auth::user()->role_id == 1){
            $request->validate([
                'rol' => ['required'],
                'nombre' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            ]);
    
            $clave = Str::random(8);
            $usuario = new User;
            $usuario->nombre = $request->nombre;
            $usuario->apellidoPaterno = $request->apellidoPaterno;
            $usuario->apellidoMaterno = $request->apellidoMaterno;
            $usuario->email = $request->email;
            $usuario->telefono = $request->telefono;
            $usuario->password = Hash::make($clave);
            $usuario->role_id = $request->rol;
            $usuario->save();
    
            $info = new \stdClass();
            $info->clave = $clave;
            $info->usuario = $usuario;
            $info->ventas = null;
    
            $usuario->notify(new NuevoPago($info));
            
            return redirect("/usuario")->with('mensaje', 'Usuario ' . $request->nombre . ' creado.');
        } else {
            return redirect('/home')->with('mensaje', 'Permiso denegado');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->role_id <= 2 or Auth::user()->id == $id){
            $usuario = User::find($id);
            return view('usuario.show', ['usuario' => $usuario]);
        } else{
            return redirect('/home');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $planes = Sueldo::all();
        $usuario = User::find($id);
        if(User::find($id)->role_id == 4){
            return view('usuario.editClient', [
                'usuario' => $usuario,
                'planes' => $planes
            ]);
        }else{
            return view('usuario.edit', ['usuario' => $usuario]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(User::find($id)->role_id == 4){
            $validatedData = $request->validate([
                'nombre' => ['required', 'string', 'max:255'],
                'apellidoPaterno' => ['required', 'string', 'max:255'],
                'email' => ['required', 'email', 'max:255', 'unique:users,email,'.$id],
                'telefono' => ['required', 'string', 'max:255'],
            ]);
            $usuario = User::find($id);
            $usuario->nombre = $request->nombre;
            $usuario->apellidoPaterno = $request->apellidoPaterno;
            $usuario->apellidoMaterno = $request->apellidoMaterno;
            $usuario->email = $request->email;
            $usuario->telefono = $request->telefono;
            $usuario->nacimiento = $request->nacimiento;
            $usuario->curp = $request->curp;
            $usuario->imss = $request->imss;
            if ($request->identificacion !== NULL){
                if($usuario->identificacion != NULL){
                    Storage::delete($usuario->identificacion);
                }
                $path = Storage::putFile('ine', $request->identificacion);
                $usuario->identificacion = $path;
            }
            
            if ($request->identificacion2 !== NULL){
                if($usuario->identificacion2 != NULL){
                    Storage::delete($usuario->identificacion2);
                }
                $path = Storage::putFile('ine', $request->identificacion2);
                $usuario->identificacion2 = $path;
            }
            
            if ($request->domicilio !== NULL){
                if($usuario->domicilio != NULL){
                    Storage::delete($usuario->domicilio);
                }
                $path = Storage::putFile('ine', $request->domicilio);
                $usuario->domicilio = $path;
            }

            if ($request->nss !== NULL){
                if($usuario->nss != NULL){
                    Storage::delete($usuario->nss);
                }
                $path = Storage::putFile('ine', $request->nss);
                $usuario->nss = $path;
            }

            if($request->rol){
                $usuario->role_id = $request->rol;
            }else{
                $usuario->role_id = 4;
            }
            $usuario->plan_id = $request->plan_id;
            $usuario->save();

            return redirect("/cliente")->with('mensaje', 'Usuario ' . $request->nombre . ' editado.');
        } else {
            $validatedData = $request->validate([
                'rol' => ['required'],
                'nombre' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            ]);

            $usuario = User::find($id);
            $usuario->nombre = $request->nombre;
            $usuario->apellidoPaterno = $request->apellidoPaterno;
            $usuario->apellidoMaterno = $request->apellidoMaterno;
            $usuario->email = $request->email;
            $usuario->telefono = $request->telefono;
            $usuario->role_id = $request->rol;
            $usuario->save();

            return redirect("/usuario")->with('mensaje', 'Usuario ' . $request->nombre . ' editado.');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::find($id);
        $nombre = $usuario->nombre;
        $usuario->forceDelete();

        return back()->with('mensaje', 'Usuario ' . $nombre . ' eliminado.');
    }
}
