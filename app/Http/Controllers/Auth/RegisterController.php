<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Venta;
use App\Sueldo;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Notifiable;
use App\Notifications\Bienvenido;
use Illuminate\Support\Facades\Cookie;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if (in_array('identificacion', $data)){
            $path = Storage::putFile('ine', $data['identificacion']);
        } else{
            $path = "";
        }

        $user = new User();
        $user->nombre = $data['nombre'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        if (isset($data['apellidoPaterno'])){
            $user->apellidoPaterno = $data['apellidoPaterno'];
        }
        if (isset($data['apellidoMaterno'])){
            $user->apellidoMaterno = $data['apellidoMaterno'];
        }
        if (isset($data['nacimiento'])){
            $user->nacimiento = $data['nacimiento'];
        }
        if (isset($data['telefono'])){
            $user->telefono = $data['telefono'];
        }
        if (isset($data['curp'])){
            $user->curp = $data['curp'];
        }
        if (isset($data['imss'])){
            $user->imss = $data['imss'];
        }
        $sueldo = Sueldo::where('codigo', $data['codigo'])->first();
        if ($sueldo){
            $user->sdi = $sueldo->sdi;
            $user->mensualidad = $sueldo->mensualidad;
            $user->plan_id = $sueldo->id;
        } else {
            $user->sdi = Sueldo::first()->sdi;
            $user->mensualidad = Sueldo::first()->mensualidad;
            $user->plan_id = Sueldo::first()->id;
        }
        $user->identificacion = $path;
        $user->save();
        
        if (Cookie::get('vendedor_id') != null){
            $venta = new Venta;
            $venta->cliente_id = $user->id;
            $venta->vendedor_id = Cookie::get('vendedor_id');
            $venta->cantidad = -1;
            $venta->desde = Carbon::now();
            $venta->hasta = Carbon::now();
            $venta->save();
        }

        $user->notify(new Bienvenido($user->nombre));

        return $user;
    }
}
