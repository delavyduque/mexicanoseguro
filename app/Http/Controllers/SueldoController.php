<?php

namespace App\Http\Controllers;

use App\Sueldo;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SueldoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('rol:2');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sueldos = Sueldo::all();

        $currency = Str::contains(env('APP_URL'), 'ahoramexicano') ? 'MXN' : 'USD';

        return view('sueldo.index', [
            'sueldos' => $sueldos,
            'currency' => $currency,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sueldo = new Sueldo;
        $sueldo->sdi = $request->inscripcion;
        $sueldo->mensualidad = $request->mensualidad;
        $sueldo->nombre = $request->nombre;
        $sueldo->codigo = $request->codigo;
        if ($request->estado == 1){
            foreach (Sueldo::all() as $s){
                $s->estado = 0;
                $s->save();
            }
            $sueldo->estado = 1;
        }
        $sueldo->save();

        return redirect("/sueldo")->with('mensaje', 'Promoción agregada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sueldo  $sueldo
     * @return \Illuminate\Http\Response
     */
    public function show(Sueldo $sueldo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sueldo  $sueldo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sueldo = Sueldo::find($id);
        if ($sueldo != null){
            return view('sueldo.editar', ['sueldo' => $sueldo]);
        } else {
            return redirect('sueldo')->with('mensaje', 'Promoción no encontrada');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sueldo  $sueldo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sueldo $sueldo)
    {
        $sueldo->sdi = $request->inscripcion;
        $sueldo->mensualidad = $request->mensualidad;
        $sueldo->nombre = $request->nombre;
        $sueldo->codigo = $request->codigo;
        if ($request->estado == 1){
            foreach (Sueldo::all() as $s){
                if ($s->id != $sueldo->id){
                    $s->estado = 0;
                    $s->save();
                }
            }
            $sueldo->estado = 1;
        } else {
            $sueldo->estado = 0;
        }
        $sueldo->save();

        $s = Sueldo::first();
        if (Sueldo::where('estado', 1)->count() == 0 && $s != null){
            $s->estado = 1;
            $s->save();
        }

        return redirect("/sueldo")->with('mensaje', 'Promoción actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sueldo  $sueldo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sueldo $sueldo)
    {
        $principal = $sueldo->estado == 1;
        $sueldo->delete();
        if ($principal && Sueldo::all()->count() > 0){
            $s = Sueldo::first();
            $s->estado = 1;
            $s->save();
        }

        return redirect("/sueldo")->with('mensaje', 'Promoción eliminada.');
    }
}
