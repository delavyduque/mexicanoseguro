<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Pregunta;
use App\Sueldo;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        switch (Auth::user()->role_id) {
            case 1:
                return redirect('/usuario');
                break;
            case 2:
                return redirect('/cliente');
                break;
            case 3:
                return redirect('/venta');
                break;
            case 5:
                return redirect('/venta');
                break;
            case 4:
                $plan = Sueldo::find(Auth::user()->plan_id) ?? Sueldo::where('estado', 1)->first() ?? Sueldo::first();
                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

                $intent = \Stripe\PaymentIntent::create([
                    'amount' => 100 * (Auth::user()->subscripciones->where("cantidad", '>=', 0)->last() == null ? $plan->mensualidad + $plan->sdi : $plan->mensualidad),
                    'currency' => Str::contains(env('APP_URL'), 'ahoramexicano') ? 'mxn' : 'usd',
                    // Verify your integration in this guide by including this parameter
                    'metadata' => ['integration_check' => 'accept_a_payment'],
                  ]);

                $preguntas = Pregunta::all();
                $currency = Str::contains(env('APP_URL'), 'ahoramexicano') ? 'MXN' : 'USD';
                
                return view('/home', [
                    'preguntas' => $preguntas,
                    'plan' => $plan,
                    'currency' => $currency,
                    'client_secret' => $intent->client_secret
                ]);
                break;
            default:
                return view('/');
                break;
        }

    }
}
