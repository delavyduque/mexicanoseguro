<?php

namespace App;

use Illuminate\Notifications\Notifiable;
//use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','apellidoPaterno','apellidoMaterno','email','password','nacimiento','telefono','curp','imss', 'sdi', 'mensualidad', 'identificacion', 'identificacion2', 'domicilio', 'nss'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function ventas()
    {
        return $this->hasMany('App\Venta', 'vendedor_id');
    }

    public function subscripciones()
    {
        return $this->hasMany('App\Venta', 'cliente_id');
    }
}
