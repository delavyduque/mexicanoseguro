<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
     public function seller() {
        return $this->hasMany('App\User');
    }

     public function buyer() {
        return $this->hasOne('App\User'); 
    }
}
