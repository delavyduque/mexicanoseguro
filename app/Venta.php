<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    public function vendedor()
    {
        return $this->belongsTo('App\User', 'vendedor_id', 'id');
    }

    public function cliente()
    {
        return $this->belongsTo('App\User', 'cliente_id', 'id');
    }
}
