<?php

namespace App;

use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;

class Sueldo extends Model
{
    public function index()
    {
        $currency = Str::contains(env('APP_URL'), 'ahoramexicano') ? 'MXN' : 'USD';
        return view('/sueldo', [
            'currency' => $currency,
        ]);
    }
}
