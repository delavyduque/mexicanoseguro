<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Notifications\ReporteDiario;
use Carbon\Carbon;

class EnviarReportes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enviar:reportes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envía reportes de altas y bajas a los administradores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $info = new \stdClass();
        $info->ventas = collect();
        foreach (User::where('role_id', 4)->get() as $u) {
            if($u->subscripciones->count() > 0){
                $primera = $u->subscripciones->where('cantidad', '>=', 0)->first();
                $ultima = $u->subscripciones->where('cantidad', '>=', 0)->sortByDesc('hasta')->first();

                if ($primera && Carbon::parse($primera->desde)->toDateString() == Carbon::yesterday()->toDateString()) {
                    $primera->tipo = "Alta";
                    $info->ventas->push($primera);
                }
                
                if ($ultima && Carbon::parse($ultima->hasta)->toDateString() == Carbon::yesterday()->toDateString()) {
                    $ultima->tipo = "Baja";
                    $info->ventas->push($ultima);
                }

                if ($primera && $ultima){

                    if ($primera->id != $ultima->id && Carbon::parse($ultima->desde)->toDateString() == Carbon::yesterday()->toDateString()) {
                        $ultima->tipo = "Renovación";
                        $info->ventas->push($ultima);
                    }
                }
            }
        }

        // https://stackoverflow.com/a/29471912/3113008
        $admins = preg_split('/\r\n|\r|\n/', setting('admin_mails'));
        foreach ($admins as $a) {
            $u = new User;
            $u->email = $a;
            $u->notify(new ReporteDiario($info));
        }
    }
}
