<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Notifications\Bienvenido;
use App\Notifications\Contacto;
use App\Sueldo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;

Auth::routes();

// Para geolocalización (proyecto abandonado)
Route::get('/', function () {
    // https://www.geeksforgeeks.org/how-to-get-visitors-country-from-their-ip-in-php/
    $ip = null;
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }
    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    $ipdat = @json_decode(file_get_contents(
        "http://www.geoplugin.net/json.gp?ip=" . $ip));

    //dd($ipdat);

    
        $plan = Sueldo::where('estado', 1)->first() ?? Sueldo::first();
        $currency = Str::contains(env('APP_URL'), 'ahoramexicano') ? 'MXN' : 'USD';
        $lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum";
        return view('welcome', [
            'plan' => $plan, 
            'currency' => $currency,
            "lorem" => $lorem
        ]);
});

Route::get('/home', 'HomeController@index')->name('home');

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

Route::put('/actualizarUsuario', function(Request $request){
    $id = Auth::user()->id;
    $request->validate([
        'nombre' => ['required', 'string', 'max:255'],
        'apellidoPaterno' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
        'telefono' => ['required', 'string', 'max:255'],
    ]);

    $usuario = User::find($id);
    $usuario->nombre = $request->nombre;
    $usuario->apellidoPaterno = $request->apellidoPaterno;
    $usuario->apellidoMaterno = $request->apellidoMaterno;
    $usuario->email = $request->email;
    $usuario->telefono = $request->telefono;
    $usuario->nacimiento = $request->nacimiento;
    $usuario->curp = $request->curp;
    $usuario->imss = $request->imss;
    if ($request->identificacion !== NULL){
        if($usuario->identificacion != NULL){
            Storage::delete($usuario->identificacion);
        }
        $path = Storage::putFile('ine', $request->identificacion);
        $usuario->identificacion = $path;
    }

    if ($request->identificacion2 !== NULL){
        if($usuario->identificacion2 != NULL){
            Storage::delete($usuario->identificacion2);
        }
        $path = Storage::putFile('ine', $request->identificacion2);
        $usuario->identificacion2 = $path;
    }

    if ($request->domicilio !== NULL){
        if($usuario->domicilio != NULL){
            Storage::delete($usuario->domicilio);
        }
        $path = Storage::putFile('ine', $request->domicilio);
        $usuario->domicilio = $path;
    }

    if ($request->nss !== NULL){
        if($usuario->nss != NULL){
            Storage::delete($usuario->nss);
        }
        $path = Storage::putFile('ine', $request->nss);
        $usuario->nss = $path;
    }

    $usuario->save();

    return Redirect::back()->with('mensaje', 'Perfil actualizado.');
})->middleware('auth');

Route::get('/cliente', function(){
    $usuarios = App\User::where('role_id', 4)->get();
    return view('usuario.index', ['usuarios' => $usuarios]);
})->middleware(['auth', 'rol:1']);

Route::resource('/usuario', 'UsuarioController');

Route::resource('/venta', 'VentaController');

Route::resource('/pregunta', 'PreguntaController');

Route::resource('/sueldo', 'SueldoController');

Route::resource('/comentario', 'ComentarioController');

Route::get('/preguntas', function(){
    return view('preguntas');
});

Route::get('/contacto', function(){
    return view('contacto');
});

Route::post('/paypal/crear', 'PaypalController@crear')->middleware(['auth', 'rol:1']);
Route::get('/paypal/lista', 'PaypalController@planes')->middleware(['auth', 'rol:1']);
Route::get('/paypal/mostrar/{id}', 'PaypalController@mostrar')->middleware(['auth', 'rol:1']);
Route::delete('/paypal/borrar/{id}', 'PaypalController@borrar')->middleware(['auth', 'rol:1']);
Route::put('/paypal/activar/{id}', 'PaypalController@activar')->middleware(['auth', 'rol:1']);
Route::post('/paypal/contrato', 'PaypalController@contrato')->middleware(['auth']);
Route::get('/execute/{success}', 'PaypalController@contratar')->middleware(['auth']);
Route::get('/successStripe', 'PaypalController@contratarStripe')->middleware(['auth']);
Route::get('/cancelStripe', 'PaypalController@contratarStripe')->middleware(['auth']);

// Route::get('/download/{path}/{file}', function ($path = null, $image = null) {
//     return Storage::download($path . '/' . $image) ?? null;
// })->middleware(['auth']);

Route::get('/down', function () {
    // https://stackoverflow.com/a/34821887/3113008
    Artisan::call('down --message="Próximamente"');
})->middleware(['auth', 'rol:1']);

Route::get('/mail/{id}', function ($id) {

    $info = new \stdClass();
    switch ($id) {
        case '1':
            $info->ventas = App\Venta::all();
            $info->usuario = App\User::all()->where('role_id', 4)->first();
            $info->clave = 'secreta';
            return (new App\Notifications\NuevoPago($info))->toMail($info);
            break;

        case '2':
            $info->nombre = "Alberto Benavides";
            return (new App\Notifications\Bienvenido($info))->toMail($info);
            break;

        default:
            # code...
            break;
    }
})->middleware(['auth', 'rol:1']);

Route::post('/contacto', function (Request $request) {

    $info = new \stdClass();
    $info->nombre = $request->nombre;
    $info->correo = $request->correo;
    $info->asunto = $request->asunto;
    $info->mensaje = $request->mensaje;

    $admins = preg_split('/\r\n|\r|\n/', setting('info_mails'));
    foreach ($admins as $a) {
        $u = new User;
        $u->email = $a;
        Notification::send($u, new Contacto($info));
    }

    return redirect('/');
});

Route::get('/contrato/{user_id}/{pdf}', function($user_id, $pdf){

    $b = $pdf == "true";
    $u = User::find($user_id);
    $pdf = PDF::loadView('contrato', [
        'usuario' => $u
    ]);

    if($b){
        return $pdf->download('Contrato Mexicano Seguro.pdf');
    }
    else{
        return view('contrato', [
            'usuario' => $u
        ]);
    }
})->middleware(['auth', 'rol:1']);;

Route::get('/registro/{vendedor_id}', function ($vendedor_id){
    if(Auth::check()){
        return redirect('/home');
    }
    $vendedor = User::find($vendedor_id);

    if ($vendedor == null || !($vendedor->role_id <= 3)) {
        return redirect('/');
    }

    Cookie::queue('vendedor_id', $vendedor_id, 30*24*60);

    return view('venta_cliente', [
        'vendedor_id' => $vendedor_id
    ]);

});

Route::post('/registro/venta/{vendedor_id}', function($vendedor_id, Request $request){
    $validatedData = $request->validate([
        'nombre' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'telefono' => ['required', 'string', 'max:255'],
        'curp' => ['required', 'string', 'max:255'],
        'nacimiento' => ['required', 'date']
    ]);

    $usuario = new User;
    $usuario->nombre = $request->nombre;
    $usuario->apellidoPaterno = $request->apellidoPaterno;
    $usuario->apellidoMaterno = $request->apellidoMaterno;
    $usuario->email = $request->email;
    $usuario->telefono = $request->telefono;
    $usuario->nacimiento = $request->nacimiento;
    $usuario->curp = $request->curp;
    $usuario->firma = $request->firma;
    $usuario->password = Hash::make($request->clave);

    $usuario->identificacion = "";
    $usuario->identificacion2 = "";
    $usuario->domicilio = "";

    $usuario->role_id = 4;

    $sueldo = Sueldo::where('codigo', $request->codigo)->first();
    if ($sueldo){
        $usuario->sdi = $sueldo->sdi;
        $usuario->mensualidad = $sueldo->mensualidad;
        $usuario->plan_id = $sueldo->id;
    } else {
        $usuario->sdi = Sueldo::first()->sdi;
        $usuario->mensualidad = Sueldo::first()->mensualidad;
        $usuario->plan_id = Sueldo::first()->id;
    }

    $usuario->save();

    $venta = new App\Venta;
    $venta->cliente_id = $usuario->id;
    $venta->vendedor_id = $vendedor_id;
    $venta->cantidad = -1;
    $venta->desde = Carbon\Carbon::now();
    $venta->hasta = Carbon\Carbon::now();
    $venta->save();

    $usuario->notify(new Bienvenido($usuario->nombre));

    return redirect("/home");
});
