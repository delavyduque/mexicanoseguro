<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('apellidoPaterno')->nullable();
            $table->string('apellidoMaterno')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->date('nacimiento')->nullable();
            $table->string('telefono')->nullable();
            $table->string('curp')->nullable();
            $table->string('imss')->nullable();
            $table->string('identificacion')->nullable();
            $table->string('identificacion2')->nullable();
            $table->string('domicilio')->nullable();
            $table->string('nss')->nullable();
            $table->binary('firma')->nullable();
            $table->float('sdi', 8, 2)->nullable();
            $table->float('mensualidad', 8, 2)->nullable();
            $table->bigInteger('role_id')->default(4);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
