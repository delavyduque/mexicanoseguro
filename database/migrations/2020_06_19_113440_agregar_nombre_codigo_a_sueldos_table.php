<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgregarNombreCodigoASueldosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sueldos', function (Blueprint $table) {
            $table->string('nombre')->nullable();
            $table->string('codigo')->nullable();
            $table->integer('estado')->default(0); // 0 por defecto; 1 principal
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sueldos', function (Blueprint $table) {
            $table->dropColumn(['nombre', 'codigo', 'estado']);
        });
    }
}
