<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            ['name' => 'Administrador']
        );
        DB::table('roles')->insert(
            ['name' => 'Supervisor']
        );
        DB::table('roles')->insert(
            ['name' => 'Vendedor']
        );
        DB::table('roles')->insert(
            ['name' => 'Cliente']
        );
        DB::table('roles')->insert(
            ['name' => 'Promotor']
        );

        DB::table('users')->insert(
            [
                'nombre' => 'Cerdito',
                'apellidoPaterno' => 'Test',
                'apellidoMaterno' => '',
                'email' => 'cerditostudios@gmail.com',
                'password' => Hash::make('password'),
                'role_id' => '1',
            ]
        );

        DB::table('users')->insert(
            [
                'nombre' => 'Monserrat',
                'apellidoPaterno' => 'Alvarez',
                'apellidoMaterno' => 'Mazatini',
                'email' => 'mmazatini@grupoquiran.com',
                'password' => "$2y$10$4X2zy.VmFnMSKzkvF2LfOeNZvVkhEFnDsZah.GN56CKloA8xz9JMu",
                'role_id' => '1',
                'nacimiento' => "1994-06-29",
                'telefono' => "8123197716",
                'sdi' => 174.0,
            ]
        );

        DB::table('sueldos')->insert(
            [
                'sdi' => 104.59,
                'mensualidad' => 150
            ]
        );
        
        // DB::table('roles')->insert(
        //     ['name' => 'Administrador']
        // );
        // DB::table('roles')->insert(
        //     ['name' => 'Supervisor']
        // );
        // DB::table('roles')->insert(
        //     ['name' => 'Vendedor']
        // );
        // DB::table('roles')->insert(
        //     ['name' => 'Cliente']
        // );

        // DB::table('roles')->insert(
        //     ['name' => 'Promotor']
        // );

        // DB::table('users')->insert(
        //     [
        //         'nombre' => 'Delavy',
        //         'apellidoPaterno' => '',
        //         'apellidoMaterno' => '',
        //         'email' => 'delavyduque@gmail.com',
        //         'password' => Hash::make('secret'),
        //         'role_id' => '1',
        //     ]
        // );

        // DB::table('users')->insert(
        //     [
        //         'nombre' => 'Gilberto',
        //         'apellidoPaterno' => '',
        //         'apellidoMaterno' => '',
        //         'email' => 'supervisor@correo.com',
        //         'password' => Hash::make('secreta'),
        //         'role_id' => '2',
        //     ]
        // );

        // DB::table('users')->insert(
        //     [
        //         'nombre' => 'Monse',
        //         'apellidoPaterno' => '',
        //         'apellidoMaterno' => '',
        //         'email' => 'vendedor@correo.com',
        //         'password' => Hash::make('secreta'),
        //         'role_id' => '3',
        //     ]
        // );

        DB::table('sueldos')->insert(
            [
                'sdi' => 104.59,
                'mensualidad' => 150
            ]
        );

        // DB::table('sueldos')->insert(
        //     [
        //         'sdi' => 108.54,
        //         'mensualidad' => 160
        //     ]
        // );

        // DB::table('sueldos')->insert(
        //     [
        //         'sdi' => 114.06,
        //         'mensualidad' => 180
        //     ]
        // );

        // DB::table('users')->insert(
        //     [
        //         'nombre' => 'Alberto',
        //         'apellidoPaterno' => 'Benavides',
        //         'apellidoMaterno' => 'Vázquez',
        //         'email' => 'cliente@correo.com',
        //         'password' => Hash::make('secreta'),
        //         'sdi' => 104.59,
        //         'mensualidad' => 150,
        //         'role_id' => '4',
        //     ]
        // );


        //$this->call(RolesTableSeeder::class);

    }
}
